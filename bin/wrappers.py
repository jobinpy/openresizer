# OpenResizer is licensed under the BSD 3-Clause License

# Copyright (c) 2019, Jobin Rezai
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
    # list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
    # this list of conditions and the following disclaimer in the documentation
    # and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
    # contributors may be used to endorse or promote products derived from
    # this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# ____________________________________________

# OpenResizer uses Pillow, a fork of the Python Image Library (PIL)

# Software License

# The Python Imaging Library (PIL) is

    # Copyright © 1997-2011 by Secret Labs AB
    # Copyright © 1995-2011 by Fredrik Lundh

# Pillow is the friendly PIL fork. It is

    # Copyright © 2010-2018 by Alex Clark and contributors

# Like PIL, Pillow is licensed under the open source PIL Software License:

# By obtaining, using, and/or copying this software and/or its associated documentation, you agree that you have read, understood, and will comply with the following terms and conditions:

# Permission to use, copy, modify, and distribute this software and its associated documentation for any purpose and without fee is hereby granted, provided that the above copyright notice appears in all copies, and that both that copyright notice and this permission notice appear in supporting documentation, and that the name of Secret Labs AB or the author not be used in advertising or publicity pertaining to distribution of the software without specific, written prior permission.

# SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.




# ---------Code starts here----------
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf
from enum import Enum, auto
from os import path

def whatis(object):
    print("\n".join(dir(object)))


class Settings:
    """
    Handles saving and loading user settings.
    
    The settings get loaded into memory when the class is instantiated.
    
    If changes are made to the settings, and you want to save the settings, 
    the 'save_to_file' method can be used to save the settings to a file.
    """
      
    def __init__(self, settings_file_path):
        
        # Make sure the file exists
        if not path.isfile(settings_file_path):
            raise FileNotFoundError
                
        # Keep track of which section we're reading.
        # Sections are like [General] or [Options], etc.
        current_section = None
        
        self.all_settings:dict = {}

        self.is_dirty = False
        self.settings_file_path = settings_file_path
        
        with open(settings_file_path) as f:
            
            # Read the settings file, one line at a time
            for line in f:
                
                # Remove all new line characters
                line:str = line.strip()
                
                # Are we reading a new section?
                if line.startswith("[") and line.endswith("]") and len(line) > 2:

                    # Get the section name, without the [] brackets
                    # example: [General] becomes General
                    line = line[1:-1]
                    
                    # Record the section name
                    current_section = line
                    
                    # If the section name is not in the dictionary, add it, with a value of a
                    # blank dictionary. The blank dictionary will later contain sub setting keys and values.
                    if current_section not in self.all_settings:
                        self.all_settings.update({current_section: {}})
                       
                    continue
                
                # Are we reading a sub setting key and value?
                elif "=" in line:
                    
                    # Make sure we have a current section
                    if not current_section:
                        raise ValueError("Setting '{}' is before a Section.".format(line))
                    
                    line_list = line.split("=", 1)
                    
                    setting_key = line_list[0]
                    setting_value = line_list[1]
                    
                                        
                    # If the value is a number, then load it as an int.
                    if setting_value.isdigit():
                        setting_value = int(setting_value)
                        
                    # If the value is 'True' or 'False', then load it as a bool.
                    elif setting_value == "True":
                        setting_value = True
                    
                    elif setting_value == "False":
                        setting_value = False
                    
                    
                    # Add to dictionary (both of the following implementations will work)
                    # self.all_settings[current_section][setting_key] = setting_value
                    self.all_settings[current_section].update({setting_key: setting_value})
                    
                                        
    def get_setting(self, section, setting, default=None):       
        
        try:
            # If the provided section is from an enum, get its string value
            
            # Example of a section that's an enum, not a string: <enum 'SettingSections'>
            if "enum" in str(type(section)):
                section = section.value            
            
            # Get the value
            value = self.all_settings[section][setting]
            
            # If the value is 'None', attempt to use the default value
            if value == "None" and default:
                value = default
  
            return value
        
        except:
            return default
    
        
    def save_setting(self, section, setting, value):
        """
        Add to or update the setting dictionary.
        This does not save the setting to a file. Saving to a file is done from a different method.
        
        Keyword arguments:
        section -- the section of the setting, without the square brackets.
        
        setting -- the setting name.
        
        value -- the value of the setting.
        """
        
        # If the provided section is from an enum, get its string value
        
        # Example of a section that's an enum, not a string: <enum 'SettingSections'>
        if "enum" in str(type(section)):
            section = section.value           
        
        # Add the section key to the settings dictionary, if it doesn't exist,
        # such as 'General'
        if section not in self.all_settings:
            # Add the section as the key, and an empty dictionary as the value
            self.all_settings.update({section: {}})
        
        # Add or update the setting and value in the settings dictionary
        self.all_settings[section].update({setting: value})
        
        self.is_dirty = True
    
    def save_to_file(self):
        """
        Save the settings to a file.
        """
    
        with open(self.settings_file_path, "w+") as f:
            
            for key, value in self.all_settings.items():
                # Key - section name (without the square brackets)
                # Value - key/value dictionary of setting and value.
                
                # Add square brackets to the section name, for the file
                key = "[" + key + "]"
                
                # Write the section name
                f.write(key + "\n")
                
                # Iterate the value dictionary
                for setting, setting_value in value.items():
                    f.write(setting + "=" + str(setting_value) + "\n")
                    
                f.write("\n")
                                
        self.is_dirty = False

    
class WidgetAppearance:
    """Methods for altering the appearance of a widget (such as setting a label to bold).""" 
           
    def set_child_label_bold(self, widget):
        # Set the label of a widget to bold
        for child in widget.get_children():
            if str(type(child)) == "<class 'gi.overrides.Gtk.Label'>":
                child.set_use_markup(True)
                child.set_label("<b>{}</b>".format(child.get_label()))

class TreeViewHelper:
    """
    Methods for interacting with a TreeView widget.
    """

    def __init__(self, tree_view):
        self.tree_view = tree_view
        
    def get_column_title(self, column_index):
        """
        Get the title text of a column.
        
        Keyword arguments:
        column_index -- The index of the column to get the title for.
        
        Returns: a string or None if the column index is not found.
        """
        
        try:
            
            column = self.tree_view.get_column(column_index)
            return column.get_title()
            
        except ex:
            return None
        
    def set_column_title(self, column_index, new_title):
        """
        Set the title text of a column.
        
        Keyword arguments:
        column_index -- The index of the column to set the title for.
        
        new_title -- the new title of the column.
        
        Returns: True if successful, False if otherwise.
        """
        
        try:
            
            column = self.tree_view.get_column(column_index)
            column.set_title(new_title)
            
            return True
            
        except ex:
            return False
        

    def get_selected_row_values(self, column_index):
        """
        Get the selected row values from a multi-select treeview.

        Keyword arguments:
        column_index -- the column index to read the value from

        Returns: a list of values if there is one selection or more; otherwise None is returned.
        """
        try:
            

            # Get the selection object
            selection = self.tree_view.get_selection()

            # Get the model and treeiter for the selections
            model, treeiter = selection.get_selected_rows()

            # No selection? return None
            if not treeiter:
                return None

            # Initialize a list that will record the item selections
            selection_list = []
            for iter in treeiter:
                selection_list.append(model[iter][column_index])

            # Return the selection list
            return selection_list

        except:
            # An error occurred, so return None
            return None

    def get_selected_row_value(self, column_index):
        """
        Get a selected row value, for a single-select treeview (not mult-select).

        Keyword arguments:
        column_index -- the column index to get the value from

        Returns: a string value if successful, or None if otherwise.
        """
        try:

            # Get the selection object
            selection = self.tree_view.get_selection()

            # Get the liststore and treeiter from the selection object

            # Note: If the TreeView was set to Gtk.SelectionMode.MULTIPLE, then here we would have had to use
            # .get_selected_rows() , but since it's a single selection, we'll use .get_selected()
            model, treeiter = selection.get_selected()

            # No selection? return None
            if not treeiter:
                return None
            else:
                # Return the selection
                return model[treeiter][column_index]

        except:
            # An error occurred, so return None
            return None


    def update_row_value(self, row_index, column_index, new_value):
        """
        Update a value in a specified row and column.
        
        Keyword arguments:
        row_index -- the row index to update
        column_index -- the column index to update
        new_value -- the new value to set
        """
        
        # Get the path pointing to the index that we want
        path = Gtk.TreePath(row_index)

        # Get the liststore of the TreeView widget

        # The liststore is also known as the model
        liststore = self.tree_view.get_model()

        # Get the treeiter using the path
        treeiter = liststore.get_iter(path)

        # Update the value
        liststore.set_value(treeiter, column_index, new_value)


    def update_row_value2(self, row_index, column_index, new_value):
        """
        Updates a value in a specified row and column (Alternate method).
        
        Keyword arguments:
        row_index -- the row index to update
        column_index -- the column index to update
        new_value -- the new value to set
        """
        # This is an alternate way of updating a TreeView (with a ListStore Model)
        # Example: liststore[1][1] = "Test"

        # Get the model of the treeview (the liststore)
        liststore = self.tree_view.get_model()

        liststore[row_index][column_index] = new_value

    def row_exists(self, text, column_index):
        """
        Check whether a given text exists in the treeview, in a specified column index.
        
        Keyword arguments:
        text -- the case-insensitive text to look for
        column_index -- the column index to check
        
        Return: bool
        """
        
        # Get the text as lowercase
        text_lcase = text.lower()
        
        # Get the model of the treeview
        liststore = self.tree_view.get_model()
        
        # Iterate the liststore
        for row in liststore:
            
            # Check for a match
            if row[column_index].lower() == text_lcase:
                return True
            
        else:
            # The iteration has been exhausted, and no match was found.
            return False
        

    def get_row_value(self, row_index, column_index):
        """
        Get a value from a specific row and column, regardless of whether it's selected or not.
        
        Keyword arguments:
        row_index -- the row index to get the value from
        column_index -- the column index to get the value from
        """
        try:

            # Get the path pointing to the row index that we want
            path = Gtk.TreePath(row_index)

            # Get the liststore (model) of the treeview
            liststore = self.tree_view.get_model()

            # Get the treeiter using the path
            treeiter = liststore.get_iter(path)

            # Get the value of the specified column
            value = liststore.get_value(treeiter, column_index)

            return value

        except ValueError:
            return None


    def get_count(self):
        """
        Get the count of rows in the treeview widget.
        
        Return: int
        """
        # Get the model (liststore) of the treeview widget
        liststore = self.tree_view.get_model()

        # Return the length of the liststore (model)
        return len(liststore)

    def add_row(self, *args):
        """Add a new row to the treeview widget.
        
        The number of *args must match the number of columns.

        Keyword arguments:
        *args -- row text as a string.
        """
        # For example: if there are 2 columns, then 2 values must be passed to this method.
        # The first value would be for the first column, second value for the second column, etc.

        # Get the liststore (model) of the treeview.

        # We use the liststore to add items to the treeview
        liststore = self.tree_view.get_model()

        # Add the new value(s). We must convert the values to a list; what's what the .append method expects.

        # Example: liststore.append(["String 1", "And another string"])
        liststore.append(list(args))

    def delete_selected_rows(self):
        """
        Remove selected rows from a multi-select treeview.
        
        Return values:
        None if there is no selection.
        True if the row(s) were deleted.
        """
        
        # Get the selection object
        selection = self.tree_view.get_selection()

        # Get the model and paths (a tuple is returned)
        model, paths = selection.get_selected_rows()

        # Make sure there is at least one selection
        if not paths:
            # There is no selection, return None
            return None

        # Delete each selection

        # We need to make sure we're looping backwards while we're removing items
        # otherwise we'll get an index/position error
        for path in reversed(paths):

            # Get the iter for the path
            iter = model.get_iter(path)

            # Remove the iter
            model.remove(iter)

        # Success, the row(s) were deleted
        return True

    def delete_selected_row(self):
        """
        Remove a selected single-row from a single-select treeview.
        
        Return values:
        False if there is no row selected
        True if the selected row was deleted.
        """

        # Get the selection object
        selection = self.tree_view.get_selection()

        # Get the liststore and treeiter from the selection object

        # Note: If the TreeView was set to Gtk.SelectionMode.MULTIPLE, then here we would have had to use
        # .get_selected_rows() , but since it's a single selection, we'll use .get_selected()
        model, treeiter = selection.get_selected()

        # No row selected? Return False
        if not treeiter:
            return False

        # Remove the row
        model.remove(treeiter)

        # Success, the row was deleted
        return True


    def delete_row(self, row_index):
        """
        Delete a row at a specific row index.
        
        Keyword arguments:
        row_index -- the row index to delete

        Return values:
        True if the row was deleted.
        False if the row was not deleted (example, the row_index did not exist).
        """


        try:

            # Get the path pointing to the row index that we want
            path = Gtk.TreePath(row_index)

            # Get the liststore (model) of the treeview
            liststore = self.tree_view.get_model()

            # Get the treeiter using the path
            treeiter = liststore.get_iter(path)

            # Remove the row index from the model
            liststore.remove(treeiter)

            return True

        except:

            return False

    def clear(self):
        """
        Delete all the items from the treeview.
        """

        # Get the liststore for the treeview
        liststore = self.tree_view.get_model()

        # Clear all the items in the liststore
        liststore.clear()


class FolderChooserBuilder:
    """Loads a Folder Chooser dialog from a glade file.
    
    Keyword arguments:
    file_chooser_glade_path -- the full path to the glade file that has the file chooser definition
    
    parent_window -- the parent that the file chooser dialog will belong to
    
    title -- the title (text) of the dialog
    
    open_button_text -- the label for the 'Open' button
    """
    


    def __init__(self, folder_chooser_glade_path, parent_window, 
                 title, open_button_text, instructions_text):

        # Initialize the file chooser window
        #Gtk.FileChooserDialog.__init__(self)      

        # Initialize the builder
        builder = Gtk.Builder()

        # Load Glade file
        builder.add_from_file(folder_chooser_glade_path)
        
        # Attach the filechooser signals (mostly for 'update-preview')
        builder.connect_signals(self)
        
        # Get the file chooser dialog
        self.file_chooser1 = builder.get_object("file_chooser1")
        
        # Selected path label
        self.lbl_selected_path = builder.get_object("lbl_selected_path")
        
        # Instructions label (to provide instructions to the user)
        self.lbl_instructions = builder.get_object("lbl_instructions")
        
        # Specify the parent of the dialog
        self.file_chooser1.set_transient_for(parent_window)

        # Set the text of the dialog window
        self.file_chooser1.set_title(title)

        # Get the 'Open' button widget
        self.btn_open=builder.get_object("btn_open")

        # Set the label of the 'Open' button
        self.btn_open.set_label(open_button_text)
        
        # Set the instructions label text
        self.lbl_instructions.set_label(instructions_text)

    def show_dialog(self):
        """Show the folder chooser dialog based on previously initialized parameters."""

        # Show the file chooser dialog and get the response
        response = self.file_chooser1.run()

        # Return the file chooser dialog's response (Gtk.ResponseType enums)
        return response

    def on_folder_changed(self, *args):
        #for arg in args:
        #    print(arg)
            
        self.lbl_selected_path.set_label(self.file_chooser1.get_current_folder())


class FileChooserBuilder:
    """Loads a File Chooser dialog from a glade file.
    
    Keyword arguments:
    file_chooser_glade_path -- the full path to the glade file that has the file chooser definition
    
    parent_window -- the parent that the file chooser dialog will belong to
    
    title -- the title (text) of the dialog
    
    open_button_text -- the label for the 'Open' button
    """
    


    def __init__(self, file_chooser_glade_path, parent_window, title, open_button_text):

        # Initialize the file chooser window
        #Gtk.FileChooserDialog.__init__(self)      

        # Initialize the builder
        builder = Gtk.Builder()

        # Load Glade file
        builder.add_from_file(file_chooser_glade_path)
        
        # Attach the filechooser signals (mostly for 'update-preview')
        builder.connect_signals(self)
        
        # Get the preview image object
        self.preview_image = builder.get_object("preview_image")
        
        # Get the file chooser dialog
        self.file_chooser1 = builder.get_object("file_chooser1")
        
        # Specify the parent of the dialog
        self.file_chooser1.set_transient_for(parent_window)

        # Set the text of the dialog window
        self.file_chooser1.set_title(title)

        # Get the 'Open' button widget
        self.btn_open=builder.get_object("btn_open")

        # Set the label of the 'Open' button
        self.btn_open.set_label(open_button_text)

        ## Create filter for JPG
        #filter_jpg = Gtk.FileFilter()
        #filter_jpg.set_name("JPG Image")
        #filter_jpg.add_mime_type("image/jpeg")
        #self.file_chooser1.add_filter(filter_jpg)
        
           

        
    def on_file_chooser_update_preview(self, file_chooser_object):
        """
        Handle the 'update-preview' signal of the file chooser dialog.
        This occurs when the selection has been changed in the file chooser dialog,
        and the preview needs to be updated.
        
        Update the preview image to reflect the newly selected file.
        
        Keyword arguments:
        file_chooser_object -- the FileChooserDialog object that called this method.
        """
        
        try:
        
            # Get the selected file name.
            selection_file_path = file_chooser_object.get_filename()
            
            # Make sure it's a file, not a folder.
            if selection_file_path and path.isfile(selection_file_path):
                # Load preview image into pixbuf
                pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(selection_file_path, 250, 250, True)
                
                # Show preview image
                self.preview_image.set_from_pixbuf(pixbuf)
            else:
                self.preview_image.clear()
                
        except:
            self.preview_image.clear()

        
    def new_filter_set(self, filter_label, file_patterns, mime_types=None):
        """Add a file filter to the file chooser dialog. Multiple file patterns and mime types
        can be specified in a single filter.
        
        Keyword arguments:
        filter_label -- the name to appear in the drop-down menu
        
        file_patterns -- file patterns to show, delimited by semicolons (example: *.jpg;*.png)
        
        mime_types -- mime types to show, delimited by semicolons
        """
        
        filter_new = Gtk.FileFilter()
        filter_new.set_name(filter_label)
        
        patterns_list = file_patterns.split(";")
        for pattern in patterns_list:
            filter_new.add_pattern(pattern)
            
        if mime_types:
            mime_list = mime_types.split(";")
            for mime in mime_list:
                filter_new.add_mime_type(mime)
            
        # Add the new filter to the file chooser dialog
        self.file_chooser1.add_filter(filter_new)

    def show_dialog(self):
        """Show the file chooser dialog based on previously initialized parameters."""

        # Show the file chooser dialog and get the response
        response = self.file_chooser1.run()

        # Return the file chooser dialog's response (Gtk.ResponseType enums)
        return response
    


class MessageDialog(Gtk.MessageDialog):
    """Display information to the user or ask the user a question."""

    class DialogType(Enum):
        INFO = auto()
        ERROR = auto()
        WARNING = auto()
        QUESTION = auto()

    class DialogButtons(Enum):
        OK = auto()
        OK_CANCEL = auto()
        YES_NO = auto()

    class DialogResponse(Enum):
        OK = auto()
        CANCEL = auto()
        YES = auto()
        NO = auto()
        DIALOG_CLOSED = auto()
        UNKNOWN = auto()



    def __init__(self, parent_window,
                        primary_text,
                        secondary_text=None,
                        dialog_type=DialogType.INFO,
                        dialog_title=None,
                        dialog_buttons=DialogButtons.OK):
        """ 
        Initialize a message dialog.
        """
        
        # Initialize Gtk.MessageDialog
        Gtk.MessageDialog.__init__(self)
        
        
        self.parent_window = parent_window
        self.primary_text = primary_text
        self.secondary_text = secondary_text
        self.dialog_type = dialog_type
        self.dialog_title = dialog_title
        self.dialog_buttons = dialog_buttons



    def show_dialog(self):
        """
        Show a message dialog to the user, using the parameters that were set when the class was initialized.
        """

        # Specify the dialog type (affects the dialog's icon)
        if self.dialog_type == self.DialogType.INFO:
            message_type = Gtk.MessageType.INFO

        elif self.dialog_type == self.DialogType.ERROR:
            message_type = Gtk.MessageType.ERROR

        elif self.dialog_type == self.DialogType.QUESTION:
            message_type = Gtk.MessageType.QUESTION

        elif self.dialog_type == self.DialogType.WARNING:
            message_type = Gtk.MessageType.WARNING

        else:
            # Default is INFO
            message_type = Gtk.MessageType.INFO

        # Specify the dialog's buttons
        if self.dialog_buttons == self.DialogButtons.OK:
            buttons_type = Gtk.ButtonsType.OK

        elif self.dialog_buttons == self.DialogButtons.OK_CANCEL:
            buttons_type = Gtk.ButtonsType.OK_CANCEL

        elif self.dialog_buttons == self.DialogButtons.YES_NO:
            buttons_type = Gtk.ButtonsType.YES_NO

        else:
            # Default to OK
            buttons_type = Gtk.ButtonsType.OK

        dialog = Gtk.MessageDialog(self.parent_window, 0, message_type,
                                   buttons_type, self.primary_text)

        # Specify secondary text?
        if self.secondary_text:
            dialog.format_secondary_text(self.secondary_text)

        # Specify the dialog's title?
        if self.dialog_title:
            dialog.set_title(self.dialog_title)

        # Get the dialog response
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            dialog_response = self.DialogResponse.OK

        elif response == Gtk.ResponseType.CANCEL:
            dialog_response = self.DialogResponse.CANCEL

        elif response == Gtk.ResponseType.YES:
            dialog_response = self.DialogResponse.YES

        elif response == Gtk.ResponseType.NO:
            dialog_response = self.DialogResponse.NO

        elif response == Gtk.ResponseType.DELETE_EVENT:
            dialog_response = self.DialogResponse.DIALOG_CLOSED

        else:
            dialog_response = self.DialogResponse.UNKNOWN

        dialog.destroy()

        return dialog_response
