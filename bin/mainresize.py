# OpenResizer is licensed under the BSD 3-Clause License

# Copyright (c) 2019, Jobin Rezai
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
    # list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
    # this list of conditions and the following disclaimer in the documentation
    # and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
    # contributors may be used to endorse or promote products derived from
    # this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# ____________________________________________

# OpenResizer uses Pillow, a fork of the Python Image Library (PIL)

# Software License

# The Python Imaging Library (PIL) is

    # Copyright © 1997-2011 by Secret Labs AB
    # Copyright © 1995-2011 by Fredrik Lundh

# Pillow is the friendly PIL fork. It is

    # Copyright © 2010-2018 by Alex Clark and contributors

# Like PIL, Pillow is licensed under the open source PIL Software License:

# By obtaining, using, and/or copying this software and/or its associated documentation, you agree that you have read, understood, and will comply with the following terms and conditions:

# Permission to use, copy, modify, and distribute this software and its associated documentation for any purpose and without fee is hereby granted, provided that the above copyright notice appears in all copies, and that both that copyright notice and this permission notice appear in supporting documentation, and that the name of Secret Labs AB or the author not be used in advertising or publicity pertaining to distribution of the software without specific, written prior permission.

# SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.




# ---------Code starts here----------

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib

from PIL import Image
from enum import Enum, auto
from os import path
import config

# Formula for aspect ratio
# adjusted width = <user-chosen height> * original width / original height
# adjusted height = <user-chosen width> * original height / original width

class ResizeInstructions(Enum):
    DO_NOT_RESIZE = auto()
    DECREASE_PERCENT = auto()
    SPECIFIC_DIMENSIONS = auto()
    SPECIFIC_DIMENSIONS_KEEP_ASPECT = auto()
    
class ResizeCondition(Enum):
    DIMENSIONS_HIGHER_THAN = auto()
    NO_CONDITION = auto()       

class SaveAsExtension(Enum):
    JPG = auto()
    PNG = auto()
    BMP = auto()
    SOURCE_EXTENSION = auto()

class FileExistsDecision(Enum):
    UNIQUE_FILENAME = auto()
    SKIP_FILE = auto()
    OVERWRITE_FILE = auto()
    
    
    
def whatis(object):
    print("\n".join(dir(object)))
    
def get_unique_file_path(suggested_full_path):
    
    # Does the provided file path exist?
    if not path.isfile(suggested_full_path):
        # The file doesn't exist, so it's ok.
        return suggested_full_path
    
    else:
        # The file path exists, keep looping until we find a unique file name that doesn't exist.
        
        # Start from counter 2
        # So that, for example, file.jpg may turn to file2.jpg
        file_counter = 2
        
        # destination_full_path_noextension -- /home/user/file
        # extension_only -- .jpg   (includes the dot)
        destination_full_path_noextension, extension_only = path.splitext(suggested_full_path)

        # Get the path without the file name
        path_only = path.dirname(suggested_full_path)
        
        while True:
            
            # Get the file name with no extension, then add the counter to it.
            # Example: file becomes file2
            file_name_no_extension = path.basename(destination_full_path_noextension) + str(file_counter)
            
            # Now add the extension to the file name
            new_file_name_with_extension = file_name_no_extension + extension_only

            # Re-create the path, but now with the new file name
            new_path = path.join(path_only, new_file_name_with_extension)
            
            # Does the destination file exist now?
            if not path.isfile(new_path):
                # The file doesn't exist, so record the new path and exit the loop
                destination_full_path = new_path
                break
            else:
                # The file exists, add to the counter so we can check again with a new file name.
                file_counter += 1
                
        return new_path
    
 
def resize_image(source_image_path, resize_instructions, 
                 resize_condition, only_if_higher_dimensions,
                 save_as_format, destination_size_info, 
                 destination_folder_path, append_resized_file_name,
                 quality, file_exists_decision, preview_mode):
    """
    Resize an image.
    
    Keyword arguments:
    source_image_path -- the path to the image file that needs to be resized.
    
    resize_instructions -- the type of resize operation to perform, based on the ResizeInstructions class.
    
    resize_condition -- resize condition, based on the ResizeCondition class.
    
    only_if_higher_dimensions -- a tuple of width, height (whether this argument is used or not depends on resize_condition)
    
    save_as_format -- the file type of the image to save as, based on the SaveAsExtension class.
    
    destination_size_info -- a tuple of width, height, or a percentage int (depending on the resize type).
    
    destination_folder_path -- the path to the folder (without the file name) to save to.
    
    append_resized_file_name -- the text to append to the destination file name.
    
    quality -- the quality of the image when saving (1 to 100).
    
    file_exists_decision -- the decision for if the destination file exists; based on FileExistsDecision.
    
    preview_mode -- bool. If it's True, then that means this resize is for preview-purposes.
    In that case, a successful return value of this method will be 'Preview OK -' followed by the full path to the resized image, rather than just the resized file name.    
    This is because the full path to the resized file, when in preview mode, will be inside a temp folder, so the caller of this method needs the whole path.
    """
        
    try:
            
        #Make sure the source file exists.
        if not path.isfile(source_image_path):
            return "Error: file not found - " + source_image_path
        
        # Make sure the destination path exists.
        if not path.isdir(destination_folder_path):
            return "Error: path not found - " + destination_folder_path
                
        
        # Intialize
        new_width = None
        new_height = None
        
        # Get the file name from the path
        source_file_name = path.basename(source_image_path)
        
        # Get the source extension
        # Return value of splitext() is a tuple, such as ('/home/garlic64/file', '.txt')
        base, source_file_extension_only = path.splitext(source_image_path)
                
        # Get the source file name that has no extension (the extension was removed from the line above)
        source_file_name_no_extension = path.basename(base)
        
        # Get the extension that we want to save as
        if save_as_format == SaveAsExtension.SOURCE_EXTENSION:
            # Same as the source image extension
            save_as_extension = source_file_extension_only
        else:
            # Set the extension based on the name of the SaveAsExtension class.
            save_as_extension = "." + save_as_format.name.lower()
        
                
        # Set destination file name
        destination_file_name = (source_file_name_no_extension + append_resized_file_name 
                                 + save_as_extension)
        
        
        #print(destination_file_name)
        
        # Record the full path to the destination file
        destination_full_path = path.join(destination_folder_path, destination_file_name)
        
        # Does the destination file already exist?
        if path.isfile(destination_full_path):
            # The destination file already exists.
            
            # What we do from here all depends on the parameter, 'file_exists_decision'.
            if file_exists_decision == FileExistsDecision.UNIQUE_FILENAME:
                # Add a number to the end of the file name, until the file doesn't exist.
                # Example: file.jpg becomes file2.jpg, or file3.jpg, etc...until the file doesn't exist.
                
                # The function below will take care of this.
                destination_full_path = get_unique_file_path(destination_full_path)
                
                # Get the latest file name
                destination_file_name = path.basename(destination_full_path)
                
            elif file_exists_decision == FileExistsDecision.SKIP_FILE:
                # Skip the image.
                return "Warning: file already exists - skipped - " + destination_file_name
        
        

        
        
        # Are we saving as the same extension of the source image?
        if save_as_format == SaveAsExtension.SOURCE_EXTENSION:
            # Get the lowercase extension, because we may end up comparing it more than once.
            source_extension_lcase = source_file_extension_only.lower()
            
            if source_extension_lcase == ".jpg":
                save_as_format = SaveAsExtension.JPG
            elif source_extension_lcase == ".png":
                save_as_format = SaveAsExtension.PNG
            elif source_extension_lcase == ".bmp":
                save_as_format = SaveAsExtension.BMP
        
        
        # Record the type of image to save, used by Pillow when saving the resized image.
        if save_as_format == SaveAsExtension.JPG:
            image_type_for_saving = "JPEG"
        elif save_as_format == SaveAsExtension.PNG:
            image_type_for_saving = "PNG"
        elif save_as_format == SaveAsExtension.BMP:
            image_type_for_saving = "BMP"
        
        # Attempt to open image
        im = Image.open(source_image_path)
        
        if not im:
            return "Error: could not read file - " + source_image_path 
        
        # Does the image have alpha? (RGBA)
        if im.mode == 'RGBA':
            # The image has alpha (PNG transparency)
            
            # If we're not saving it as a PNG, convert it to a RGB from an RGBA.
            # If we don't do this, Pillow will return an error.
            if not save_as_format == SaveAsExtension.PNG:
                im = im.convert('RGB')
        
        # Get the current size of the image
        source_width, source_height = im.size
        
        # Should we only resize this image if it exceeds a specific set of dimensions? (but only if it's NOT set to 'Do not resize')
        if resize_condition == ResizeCondition.DIMENSIONS_HIGHER_THAN and resize_instructions != ResizeInstructions.DO_NOT_RESIZE:
            # The option has been set to only resize this image if it's larger than specific dimensions.
            if source_width <= only_if_higher_dimensions[0] and source_height <= only_if_higher_dimensions[1]:
                # There is no need to resize this image, it's small enough.
                return "Warning: dimensions are equal or below the size condition - " + source_image_path
        
        
        # Resize to specific dimensions, no aspect ratio?
        if resize_instructions == ResizeInstructions.SPECIFIC_DIMENSIONS or resize_instructions == ResizeInstructions.SPECIFIC_DIMENSIONS_KEEP_ASPECT:
            # Resize to specific dimensions
            new_width = destination_size_info[0]
            new_height = destination_size_info[1]
        
            
        elif resize_instructions == ResizeInstructions.DECREASE_PERCENT:
            # Decrease the width / height by the percentage specified in 'destination_size_info'.
            new_width = int(round(source_width - (source_width * (destination_size_info / 100))))
            new_height = int(round(source_height - (source_height * (destination_size_info / 100))))            
                
        
            
        # Resize image, unless the resize instruction is set to 'Do not resize'
        if not resize_instructions == ResizeInstructions.DO_NOT_RESIZE:
            # We're going to resize, because it's not set to 'Do not resize'
                        
            if new_width and new_height:
                
                # Resize to specific dimensions, without aspect ratio?
                if resize_instructions == ResizeInstructions.SPECIFIC_DIMENSIONS:
                    # Resize, without aspect ratio
                    im = im.resize((new_width, new_height), Image.ANTIALIAS)
                else:
                    # Resize, with aspect ratio
                    im.thumbnail((new_width, new_height), Image.ANTIALIAS)
            
            else:
                
                # There is an issue with the new width and/or new height
                
                # new_width and/or new_height are zero (0)
                # No resize has taken place, even though we were asked to do a resize
                
                # This will happen if the user is attempting to decrease the size of the image by-percentage,
                # but the source image is too small to decrease it by the asked-percentage.
                # Example: an image 1x1 in dimensions cannot be made smaller by 99 percent.
                return f"Error: {source_image_path} cannot resize to {new_width} width by {new_height} height"
                
                
            
        # Save the image to a file, if we have an actual image
        if im:
            im.save(destination_full_path, image_type_for_saving, quality=quality)
            
            if preview_mode:
                return "Preview OK - " +  destination_full_path
            else:
                return "OK - " +  destination_file_name
        else:
            return "Error: " +  destination_file_name
        
    except BaseException as e:

        return "Error: " + repr(e) + " - " + destination_file_name





