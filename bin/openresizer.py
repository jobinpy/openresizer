#!/usr/bin/env python3 
# OpenResizer is licensed under the BSD 3-Clause License

# Copyright (c) 2019, Jobin Rezai
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
    # list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
    # this list of conditions and the following disclaimer in the documentation
    # and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
    # contributors may be used to endorse or promote products derived from
    # this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# ____________________________________________

# OpenResizer uses Pillow, a fork of the Python Image Library (PIL)

# Software License

# The Python Imaging Library (PIL) is

    # Copyright © 1997-2011 by Secret Labs AB
    # Copyright © 1995-2011 by Fredrik Lundh

# Pillow is the friendly PIL fork. It is

    # Copyright © 2010-2018 by Alex Clark and contributors

# Like PIL, Pillow is licensed under the open source PIL Software License:

# By obtaining, using, and/or copying this software and/or its associated documentation, you agree that you have read, understood, and will comply with the following terms and conditions:

# Permission to use, copy, modify, and distribute this software and its associated documentation for any purpose and without fee is hereby granted, provided that the above copyright notice appears in all copies, and that both that copyright notice and this permission notice appear in supporting documentation, and that the name of Secret Labs AB or the author not be used in advertising or publicity pertaining to distribution of the software without specific, written prior permission.

# SECRET LABS AB AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL SECRET LABS AB OR THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.




# ---------Code starts here----------


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib, GdkPixbuf
from wrappers import TreeViewHelper
from wrappers import MessageDialog
from wrappers import FileChooserBuilder
from wrappers import FolderChooserBuilder
from wrappers import WidgetAppearance
from wrappers import Settings
from os import path, remove, cpu_count
from tempfile import TemporaryDirectory
from urllib.parse import unquote
from enum import Enum, auto
from collections import namedtuple
from mainresize import ResizeInstructions
from mainresize import ResizeCondition
from mainresize import SaveAsExtension
from mainresize import FileExistsDecision
from mainresize import resize_image
from multiprocessing import Pool
from multiprocessing import freeze_support # for the generated cx_Freeze .exe to work
from functools import partial
from threading import Thread
from time import time
from sys import modules # Used for getting the current script's directory
from pathlib import Path # Used for replacing Unix-style forward-slashes in a path to back-slashes in Windows (for drag and drop)
from platform import system as osname
from webbrowser import open as web_open # For opening the website link in the about dialog (for some reason the GtkLinkButton doesn't open the URL in Windows)
import subprocess # used for opening the save folder in the OS's file manager

import glob
import config

from os import environ # Used for getting the $SNAP_USER_DATA path, or the Local App Data folder (Windows), if applicable
from shutil import copyfile

## Used for debugging, to get the Python version
#from sys import version
#print(sys.version)



def whatis(object):
    print("\n".join(dir(object)))


# from: https://stackoverflow.com/questions/14996453/python-libraries-to-calculate-human-readable-filesize-from-bytes
def humansize(nbytes):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    i = 0
    while nbytes >= 1024 and i < len(suffixes)-1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])



class GladeWindowType(Enum):
    
    MAIN_WINDOW = auto()
    PREVIEW_WINDOW = auto()
    FILE_CHOOSER_DIALOG = auto()
    FOLDER_CHOOSER_DIALOG = auto()
    QUICK_GUIDE = auto()
    ABOUT_WINDOW = auto()
    PROGRESS_WINDOW = auto()

class CurrentOS(Enum):
    RUNNING_LINUX = auto()
    RUNNING_WINDOWS = auto()


def get_glade_file_path(glade_window_type):
    """
    Get the file path to a specific type of Glade file.
    
    Keyword arguments:
    glade_window_type -- the type of glade window to get the file path for, based on the GladeWindowType class.
    """
    return_path = None
    glade_folder_name = "glade_files"
    
    # Get the full path of where the current module is
    # Example: /snap/openresizer/x1/bin/openresizer.py
    
    # The full path only seems to matter in Linux Snaps.
    # Don't attempt to run the line below in Windows, 
    # because it'll throw an exception: module '__main__' has no attribute '__file__'
    if DefaultSettings.PLATFORM == CurrentOS.RUNNING_LINUX:
        module_full_path = modules[__name__].__file__
        
    elif DefaultSettings.PLATFORM == CurrentOS.RUNNING_WINDOWS:
        # We only use this variable in Linux, not Windows, but we still need to initialize it.
        module_full_path = ""
    
    # Get the path only, of where the current module is
    # Example: /snap/openresizer/x1/bin
    module_path_only = path.dirname(module_full_path)
    
    print(f"Debug: Module full path: {module_full_path}")
    print(f"Debug: Module path only: {module_path_only}")
    
    # We get the module's path, so we can provide an absolute path to the glade files.
    
    if glade_window_type == GladeWindowType.MAIN_WINDOW:
        return_path = path.join(module_path_only, glade_folder_name, "resizer.glade")
    
    elif glade_window_type == GladeWindowType.ABOUT_WINDOW:
        return_path = path.join(module_path_only, glade_folder_name, "about_dialog.glade")
    
    elif glade_window_type == GladeWindowType.FILE_CHOOSER_DIALOG:
        return_path = path.join(module_path_only, glade_folder_name, "file_chooser_dialog.glade")
    
    elif glade_window_type == GladeWindowType.FOLDER_CHOOSER_DIALOG:
        return_path = path.join(module_path_only, glade_folder_name, "folder_chooser_dialog.glade")

    elif glade_window_type == GladeWindowType.PREVIEW_WINDOW:
        return_path = path.join(module_path_only, glade_folder_name, "preview_side_by_side.glade")
        
    elif glade_window_type == GladeWindowType.PROGRESS_WINDOW:
        return_path = path.join(module_path_only, glade_folder_name, "progress_dialog_v2.glade")
        
    elif glade_window_type == GladeWindowType.QUICK_GUIDE:
        return_path = path.join(module_path_only, glade_folder_name, "quick_guide.glade")
        
    print(f"Debug: GLADE PATH: {return_path}")
        
    return return_path

class SettingSections(Enum):
    GENERAL = "General"
    OPTIONS = "Options"
    FILENAME = "FileName"
    MAINWINDOW = "MainWindow"
    
class DefaultSettings:
    DEFAULT_WINDOW_WIDTH = 630
    DEFAULT_WINDOW_HEIGHT = 514
    
    # Keeps record of which OS the script is running in
    PLATFORM = None
    
class ProcessorUsage(Enum):
    SINGLE_CPU = auto()
    MULTI_CPU = auto()
    

class PreviewCompareWindow:
    
    def __init__(self, original_image_path, resized_image_path):
        
        temp_folder = "temp"
        glade_path = get_glade_file_path(GladeWindowType.PREVIEW_WINDOW)
        
        
        # Initialize the GUI builder
        builder = Gtk.Builder()
        builder.add_from_file(glade_path)
        
        # Get the preview window
        self.preview_window_sidebyside = builder.get_object("preview_window_sidebyside")
        
        # Set the transient for the preview window
        self.preview_window_sidebyside.set_transient_for(window.main_window)
        
        # Get the original GtkImage
        self.image_original = builder.get_object("image_original")
        # Get the resized GtkImage
        self.image_resized = builder.get_object("image_resized")
        
        # Record the original and resized image paths (temp resized path)
        self.original_image_path = original_image_path
        self.resized_image_path = resized_image_path
        
        # Get original version labels (file name, file size, dimensions)
        self.lbl_original_filename = builder.get_object("lbl_original_filename")
        self.lbl_original_filesize = builder.get_object("lbl_original_filesize")
        self.lbl_original_dimensions = builder.get_object("lbl_original_dimensions")
        
        # Get resized version labels (file name, file size, dimensions)
        self.lbl_resized_filename = builder.get_object("lbl_resized_filename")
        self.lbl_resized_filesize = builder.get_object("lbl_resized_filesize")
        self.lbl_resized_dimensions = builder.get_object("lbl_resized_dimensions")
        
        
        
    def show_preview(self):
        
        try:  
            
            # Make sure the source image path exists
            if not path.isfile(self.original_image_path):
                message = MessageDialog(window.main_window, "Source image not found.\n" + self.original_image_path, 
                                        None, MessageDialog.DialogType.ERROR, "Source Image Not Found", 
                                        MessageDialog.DialogButtons.OK)
                
                response = message.show_dialog()
                
                return
            
            
            # Make sure the resized image path exists
            if not path.isfile(self.resized_image_path):
                message = MessageDialog(window.main_window, "Resized image not found.\n" + self.resized_image_path, 
                                        None, MessageDialog.DialogType.ERROR, "Resized Image Not Found", 
                                        MessageDialog.DialogButtons.OK)
                
                response = message.show_dialog()
                
                return            
            
            
            pixbuf_original_image = GdkPixbuf.Pixbuf.new_from_file(self.original_image_path)
            pixbuf_resized_image = GdkPixbuf.Pixbuf.new_from_file(self.resized_image_path)
            
            dimensions_original = str(pixbuf_original_image.get_width()) + " x " + str(pixbuf_original_image.get_height())
            dimensions_resized = str(pixbuf_resized_image.get_width()) + " x " + str(pixbuf_resized_image.get_height())

        
            # Load the original image
            self.image_original.set_from_pixbuf(pixbuf_original_image)  #set_from_file(self.original_image_path)
            
            # Load the resized image
            self.image_resized.set_from_pixbuf(pixbuf_resized_image)  #set_from_file(self.resized_image_path)
            
            # Get the file sizes of both images
            original_file_size = path.getsize(self.original_image_path)
            resized_file_size = path.getsize(self.resized_image_path)
            
            # Show file sizes in window
            self.lbl_original_filesize.set_label(humansize(original_file_size))
            self.lbl_resized_filesize.set_label(humansize(resized_file_size))
            
            
            # Show the dimensions of the images (original and resized)
            self.lbl_original_dimensions.set_label(dimensions_original)
            self.lbl_resized_dimensions.set_label(dimensions_resized)
            
            # Get filenames only
            file_name_original = path.basename(self.original_image_path)
            file_name_resized = path.basename(self.resized_image_path)
            
            # Show the file names to the user
            self.lbl_original_filename.set_label(file_name_original)
            self.lbl_resized_filename.set_label(file_name_resized)
            
            
            # Show the window
            self.preview_window_sidebyside.show_all()
            
            
            return True
        
            
        except BaseException as e:
            
            message = MessageDialog(window.main_window, repr(e), 
                                    None, MessageDialog.DialogType.ERROR, "Preview Error", 
                                    MessageDialog.DialogButtons.OK)
            
            response = message.show_dialog()
            
            return None
        
        finally:         
            # Clean-up temp directory, if a temp directory was created.
            
            if config.preview_temp_dir:
                # Now delete the temporary folder where the resized image was saved - we no longer need it.
                config.preview_temp_dir.cleanup()
                config.preview_temp_dir = None

class ProgressWindow:
    
    class IconNames:
        ERROR_ICON = "dialog-error" #"dialog-error"
        OK_ICON = "gtk-apply" #"dialog-ok"
        WARNING_ICON = "dialog-warning" # warning icon
        INFO_ICON = "dialog-information"

    class ButtonPurpose(Enum):
        OK_BUTTON = auto()
        CANCEL_BUTTON = auto()
           
    
    def __init__(self, glade_ui_path, main_window, total_image_count, destination_path):
        
        # Instantiate builder, so we can load the progress UI
        builder = Gtk.Builder()
        builder.add_from_file(glade_ui_path)
        
        # Attach the signals to this class
        builder.connect_signals(self)
        
        # Get the progress window
        self.progress_window = builder.get_object("progress_window")
        
        # Get the progress bar
        self.progress_bar = builder.get_object("progress_bar")
        
        # Get the scrolled-window, so we can autoscroll it to the bottom
        # used by the on_auto_scroll signal handler.
        self.scrolled_window = builder.get_object("scrolled_window")
        
        # Get the success label
        self.lbl_success = builder.get_object("lbl_success")
        
        # Get the error label
        self.lbl_errors = builder.get_object("lbl_errors")
        
        # Get the total label
        self.lbl_total = builder.get_object("lbl_total")
        
        # Get the warning label
        self.lbl_warnings = builder.get_object("lbl_warnings")
        
        # Show the total image count
        self.lbl_total.set_label("Total Files: " + str(total_image_count))
        
        # Get the destination path (save path), so when the button 'Go to Save Folder' is clicked, we know where to go.
        self.destination_path = destination_path
        
        ## Get the textview
        #self.txt_log = builder.get_object("txt_log")
        
        ## Get the textbuffer of the textview
        #self.text_buffer = self.txt_log.get_buffer()
        
        # Get the OK (checkmark) image
        self.ok_image = builder.get_object("ok_image")
        
        # Get the Cancel button
        self.btn_cancel = builder.get_object("btn_cancel")
        
        # Go to Save Folder link button
        self.link_gotosavefolder = builder.get_object("link_gotosavefolder")
        # Get the image for the link button
        image_savefolder = builder.get_object("image_savefolder")
        self.link_gotosavefolder.set_label("Open Save Folder...")
        self.link_gotosavefolder.set_image(image_savefolder)
        self.link_gotosavefolder.set_always_show_image (True)
        
        # If there is no destination folder specified (which will be the case when an image is being previewed),
        # then disable the link 'Open save folder...'
        if not destination_path or not path.isdir(destination_path):
            self.link_gotosavefolder.set_visible(False)
        
        # Button purpose
        self.btn_cancel_purpose = self.ButtonPurpose.CANCEL_BUTTON
        
        # Get the status label
        self.lbl_status = builder.get_object("lbl_status")
        
        # Get the liststore for the logs
        self.liststore_logs = builder.get_object("liststore_logs")
        # Get the filter
        self.filter_logs = builder.get_object("filter_logs")
        # Set the filter function for the logs
        self.filter_logs.set_visible_func(self.log_filter_func)
        
        # Set the current log filter (default to All)
        self.current_filter = "All" # Other filters are: Er, OK, Wa, In
        
        # Set the trasient window, so the main window can't be interacted with
        # while the progress window is being displayed.
        self.progress_window.set_transient_for(main_window)
        
        # No text has been set, but when we set this flag, it'll automatically
        # show the percentage based on the fraction that gets set.
        self.progress_bar.set_show_text(True)
        
        # Set cancel flag
        self.cancel_process = False
        
        # Set window closing flag
        self.window_closing = False

        
        
    def show_window(self):

        # Show the progress window
        self.progress_window.show()
        
        # Note: we use .show() instead of .show_all(), because if the LinkButton 'open save folder...' is 
        # set to be Not visible, then .show_all() will show it any way, regardless of whether we use set_visible(False) or not.
        # But when we use .show() for the window, the LinkButton will stay hidden, if it's set to be hidden.
        
    def on_delete_event(self, window, event):
        # Handler for when the window has been requested to close manually using the X.

        # In this case, make it just like the Cancel or OK button was clicked
        self.cancel_or_ok()
        
        # Don't allow the window to close here, because the cancel_or_ok() method will close
        # the window there.
        return True
        
    
    def on_cancel_button_click(self, widget):
        # Occurs when the 'Cancel' or 'OK' button is clicked in the progress window.
        
        self.cancel_or_ok()
            
        
    def cancel_or_ok(self):
        
        # Is there a cancel already in progress? don't do anything.
        if self.cancel_process:
            return
        
        # OK button? just close the progress window.
        if self.btn_cancel_purpose == self.ButtonPurpose.OK_BUTTON:
            self.progress_window.destroy()
            
        else:
            # Cancel button
            
            # Update
            self.append_log("Information: Cancelling...")
   
            # Set cancel flag
            self.cancel_process = True
            
            # Set closing flag so other methods know that the window is about to close.
            self.window_closing = True            
        
            # Disable the Cancel button
            self.btn_cancel.set_sensitive(False)
            
            # Close the window immediately
            # self.progress_window.destroy()        


    def on_link_gotosavefolder_activate_link(self, widget):
        
        try:
    
            if DefaultSettings.PLATFORM == CurrentOS.RUNNING_LINUX:
                subprocess.Popen(["xdg-open", self.destination_path])
                
                ## Convert the path to a uri that Gio can use
                #uri = GLib.filename_to_uri(self.destination_path)
                
                ## Open folder
                #Gio.app_info_launch_default_for_uri(uri, None)
                
            elif DefaultSettings.PLATFORM == CurrentOS.RUNNING_WINDOWS:
                subprocess.Popen(["explorer", self.destination_path])
                
        except BaseException as e:
            print(f"Debug: Could not launch default file manager to open path: {self.destination_path}")
        
        finally:
    
            # returning True is needed here, otherwise the LinkButton will show a GTK warning
            # of the URI being Null (because we're not using the URI of the LinkButton)
            return True

    def on_progress_window_key_press_event(self, window, event_key):
        
        # Get the key that was pressed, as a string
        key_pressed = Gdk.keyval_name(event_key.keyval)
        
        if key_pressed == "Escape":
            self.cancel_or_ok()
        

    def on_combo_filters_changed(self, widget):
        
        
        # Get the selected index
        selected_index = widget.get_active()
        
        # print(selected_index)
        
        # Get the model of the combobox
        model = widget.get_model()
        
        selected_text = model[selected_index][0]
        
        if selected_text == "Errors":
            self.current_filter = "Er"
            
        elif selected_text == "Successes":
            self.current_filter = "OK"
            
        elif selected_text == "Warnings":
            self.current_filter = "Wa"
            
        elif selected_text == "Information":
            self.current_filter = "In"
            
        else:
            self.current_filter = "All"
                
        self.filter_logs.refilter()
            
        
    def log_filter_func(self, model, iter, data):
        
        
        if self.current_filter == "All" or self.current_filter is None:
            return True
        else:
            
            return model[iter][1][0:2] == self.current_filter



    def show_error_message(self, message, error_title):
        # This is used when a serious error occurs, even before the resize process has begun,
        # such as when the destination folder cannot be found.
        
        
        # Show the error message
        self.append_log("Error: " + message)
        
        # Change the Cancel button OK
        self.change_to_ok_button()
        
        # Set the status text to the title of the error.
        self.lbl_status.set_text(error_title)
        
    def change_to_ok_button(self):
        
        # Change the Cancel button to OK
        self.btn_cancel.set_label("OK")
        self.btn_cancel.set_image(self.ok_image)
        
        # Set the flag for the button's purpose
        self.btn_cancel_purpose = self.ButtonPurpose.OK_BUTTON
        

    def update_progress(self, progress_so_far, total_count, success_count, error_count, 
                        warning_count, result):
        #print("So far: {}  Total: {}".format(str(progress_so_far), str(total_count)))
        #print("Result: " + result)
        
        self.lbl_success.set_text("Success: " + str(success_count))
        self.lbl_errors.set_text("Errors: " + str(error_count))
        self.lbl_warnings.set_text("Warnings: " + str(warning_count))
        self.append_log(result)
    
        # If we have the result of a preview attempt, and it's not successful, 
        # then cleanup the temp directory.
        if not result.startswith("Preview OK") and config.preview_temp_dir:
            config.preview_temp_dir.cleanup()
            config.preview_temp_dir = None

    
        new_value = progress_so_far / total_count
        self.progress_bar.set_fraction(new_value)
        
    def progress_finished(self, start_time, end_time, destination_path, 
                          preview_source_image_path, preview_resized_full_path):
        # The resize process has finished.
        
        # Show this in the progress window.
        
        # Change the 'Cancel' button to OK
        self.change_to_ok_button()
        
        self.lbl_status.set_label("Finished")
        
        finish_time = round(end_time - start_time, 2)
        
        self.append_log("Information: finished - " + destination_path)
        self.append_log("Information: time elapsed: " + str(finish_time) + " seconds")
        
        
        # If the full path to the resized preview file has been provided, try to load the
        # resized image, then try to delete the resized image, because it's only a preview.
        if preview_source_image_path and preview_resized_full_path:
            preview = PreviewCompareWindow(preview_source_image_path, preview_resized_full_path)
            
            # Show the preview window and load the images (original image and resized image)
            if preview.show_preview():            
                self.progress_window.destroy()
    
    def close_progress_window(self):
        # Closes the progress window. We have this method so that a separate thread can call this.
        # using GLib.idle_add()
        self.progress_window.destroy()
        
    def on_auto_scroll(self, *args):
        # Scroll 'scrolled_window' to bottom
        adj = self.scrolled_window.get_vadjustment()
        adj.set_value(adj.get_upper())
        
    def append_log(self, log_text):
        
        # Update the log text in the provided text buffer.
        
        # This is used in the progress window UI.
        
        # If the window is closing, don't do anything in this method.
        if self.window_closing:
            return
        
        
        # Set the icon name, which gets used by the treeview to display an icon
        if log_text:
            if log_text.startswith("OK ") or log_text.startswith("Preview OK "):
                icon_name = self.IconNames.OK_ICON
            elif log_text.startswith("Info"):
                icon_name = self.IconNames.INFO_ICON
            elif log_text.startswith("Error"):
                icon_name = self.IconNames.ERROR_ICON
            elif log_text.startswith("Warning"):
                icon_name = self.IconNames.WARNING_ICON
            else:
                icon_name = self.IconNames.WARNING_ICON
                
        
        # Add log text, including the icon name, to the liststore
        self.liststore_logs.append([icon_name, log_text])
            
        
        ## Get the ending iter
        #end_iter = text_buffer.get_end_iter()
        
        ## Scroll to bottom
        #adj = self.scrolled_window.get_vadjustment()
        #adj.set_value(adj.get_upper())              
        
        ## Insert text
        #text_buffer.insert(end_iter, log_text + "\n")
        

class MainWindow:
    def __init__(self):

        # List of supported extensions (for drag and drop operations)
        self.supported_formats = [".jpg", ".png", ".bmp"]
               
        builder = Gtk.Builder()
        builder.add_from_file(get_glade_file_path(GladeWindowType.MAIN_WINDOW))


        # Attach the signals
        builder.connect_signals(self)

        # Get the main window
        self.main_window = builder.get_object("main_window")
                
        # Get the treeview widget
        self.gtk_treeview = builder.get_object("gtk_treeview")
        
        # Enable drag and drop on the treeview widget
        #enable_model_drag_dest
        targets = [("text/uri-list", 0, 0)]
        self.gtk_treeview.enable_model_drag_dest(targets, Gdk.DragAction.COPY)
  
        # Instantiate a wrapper object
        self.wrapper_treeview = TreeViewHelper(self.gtk_treeview)
        
        # Get File menu widgets, used for the context menu
        self.mnu_file = builder.get_object("mnu_file")
        
        
        # We're going to hide the following two menus if a right-click menu is shown
        self.menu_seperator1 = builder.get_object("menu_seperator1")
        self.menu_quit = builder.get_object("menu_quit")
        
        # Get radio buttons, 'Do not resize', 'Resize by pixel' and 'Resize by percent', 
        # so that we can set their labels to bold
        self.radiobutton_donotresize = builder.get_object("radiobutton_donotresize")
        self.radiobutton_resize_by_pixel = builder.get_object("radiobutton_resize_by_pixel")
        self.radiobutton_resize_by_percent = builder.get_object("radiobutton_resize_by_percent")
        
                
        # Set the radio buttons' labels to bold
        appearance = WidgetAppearance()
        appearance.set_child_label_bold(self.radiobutton_donotresize)
        appearance.set_child_label_bold(self.radiobutton_resize_by_pixel)
        appearance.set_child_label_bold(self.radiobutton_resize_by_percent)
        
        # Get the option frames, so we can enable/disable the frames as needed.
        self.frame_pixel = builder.get_object("frame_pixel")
        self.frame_percentage = builder.get_object("frame_percentage")
        self.frame_condition = builder.get_object("frame_condition")
        
        # Get the checkbutton 'Only resize image(s) if the dimensions are higher than:'
        # and the spinbuttons (width/height)
        self.chk_if_higher = builder.get_object("chk_if_higher")
        self.spin_ifhigher_width = builder.get_object("spin_ifhigher_width")
        self.spin_ifhigher_height = builder.get_object("spin_ifhigher_height")
        
        
        # Get the radiobutton for single/multi processor usage
        self.radiobutton_single_cpu = builder.get_object("radiobutton_single_cpu")
        self.radiobutton_multi_cpu = builder.get_object("radiobutton_multi_cpu")
        
        # File Name tab widgets
        self.radiobutton_save_as_unique = builder.get_object("radiobutton_save_as_unique")
        self.radiobutton_skip_image = builder.get_object("radiobutton_skip_image")
        self.radiobutton_overwrite_destination = builder.get_object("radiobutton_overwrite_destination")
        self.entry_resized_file_name = builder.get_object("entry_resized_file_name")
        self.lbl_filename_example = builder.get_object("lbl_filename_example")
    
        # Show the main window
        #self.main_window.show_all()
        
        # Record the default expected path to the settings file, if it exists.
        # If it doesn't exist, None is returned
        self.default_settings_full_path = self.get_config_file_path()

        print(f"Debug: Config file save-to path read as: {self.default_settings_full_path}")
        
        # No config file? tell the user
        if not self.default_settings_full_path or not path.isfile(self.default_settings_full_path):
            
            # Show the main window, in case the user wants to exit the app
            self.main_window.show_all()            
            
            MsgBox = MessageDialog(self.main_window, "Config file not found!",
                                   """Continuing is not recommended until the application 
has been reinstalled and this error no longer appears.""", 
                                    MessageDialog.DialogType.ERROR,
                                   "Config Error", MessageDialog.DialogButtons.OK)
            
            response = MsgBox.show_dialog()
        else:
        
            self.Settings = Settings(self.default_settings_full_path)


            
            # Should the main window be maximized?
            window_state = self.Settings.get_setting(SettingSections.MAINWINDOW, "state", default='normal')
            if window_state == "maximized":
                self.main_window.maximize()
            else:
                # Get the size of the window
                self.window_width = self.Settings.get_setting(SettingSections.MAINWINDOW, "width", default=DefaultSettings.DEFAULT_WINDOW_WIDTH)
                self.window_height = self.Settings.get_setting(SettingSections.MAINWINDOW, "height", default=DefaultSettings.DEFAULT_WINDOW_HEIGHT)
                
                # Set the size of the window based on the saved settings
                self.main_window.set_default_size(self.window_width, self.window_height)                
            
            # We need to have these two variables so that when the main window is about to close, the delete_event signal handler
            # will populate the width/height of the window to these two variables.
            # (in case the user has adjusted it), so we can save it to the config file.
            
            # refresh_settings_dictionary() is run when file(s) are about to be resized 
            # and also when the main window is about to be closed.
            
            # If the user is resizing images, then these two variables don't matter. That's why we initialize them to None here.
            self.adjusted_width = None
            self.adjusted_height = None
            
            # Show the main window
            
            # The main window gets shown 'after' the default size has been set (above).
            # If we had done it the other way around, the user's window size setting 
            # would not reflect on the main window.
            self.main_window.show_all()            

            # Disable both option frames (Resize by pixel and Resize by percent)

            # This is until we know which one we want to enable initially
            self.frame_percentage.set_sensitive(False)
            self.frame_pixel.set_sensitive(False)
            
            #self.radiobutton_resize_by_percent.set_state_flags(Gtk.StateFlags.NORMAL, True)
            #self.radiobutton_resize_by_pixel.set_state_flags(Gtk.StateFlags.NORMAL, True)

            #print(self.radiobutton_resize_by_pixel.get_state_flags())
            
            # Get the last 'add_folder'
            self.initial_add_folder = self.Settings.get_setting(SettingSections.GENERAL, 
                                                                "initial_add_folder")
            # A default value will be a string "None", which means a folder hasn't been selected yet.
            if self.initial_add_folder == "None":
                self.initial_add_folder = None
                
            
            # Get the last 'save_folder'
            self.initial_save_folder = self.Settings.get_setting(SettingSections.GENERAL,
                                                                 "initial_save_folder")
            # A default value will be a string "None", which means a folder hasn't been selected yet.
            if self.initial_save_folder == "None":
                self.initial_save_folder = None

            # Apply 'resize_type' config value to the GUI.
            value = self.Settings.get_setting(SettingSections.OPTIONS, "resize_type", 
                                              default="percent")
            if value == "percent":
                
                # Check (toggle on) the 'Percent' radiobutton
                self.radiobutton_resize_by_percent.set_active(True)
                
                # We don't need to manually run the 'Percent' radiobutton's 'toggled' event,
                # because when we set it to active, from the code above, it'll automatically run
                # the 'toggled' event for the radio button. This is because the 'percent' radio button
                # is not toggled-on by default, so its event fires.
            
            elif value == "pixel":
                
                # Run the radio button's 'toggled' event, because the 'Pixel' radio button
                # is selected by default, so the event won't fire if we set it to active,
                # because it's actually already selected
                self.on_radiobutton_resize_type_changed(self.radiobutton_resize_by_pixel)
            
            elif value == "noresize":
                
                # Check (toggle on) the 'Do not resize' radiobutton
                self.radiobutton_donotresize.set_active(True)
            
        
            # Apply 'resize_to_width' config value to the GUI.
            value = self.Settings.get_setting(SettingSections.OPTIONS, "resize_to_width",
                                              default=640)
            self.spin_resize_width = builder.get_object("spin_resize_width")
            self.spin_resize_width.set_value(value)
 
            # Apply 'resize_to_height' config value to the GUI.
            value = self.Settings.get_setting(SettingSections.OPTIONS, "resize_to_height",
                                              default=480)
            self.spin_resize_height = builder.get_object("spin_resize_height")
            self.spin_resize_height.set_value(value)
            
            # Apply 'decrease_size_by' spinner (percent) config value to the GUI.
            value = self.Settings.get_setting(SettingSections.OPTIONS, "decrease_size_by",
                                              default=50)
            self.spin_decrease_size_percent = builder.get_object("spin_decrease_size_percent")
            self.spin_decrease_size_percent.set_value(value)            
            
                        
            # Activate
            self.spin_decrease_size_percent.set_sensitive(True)                
            
            
            # Keep Aspect Ratio checkbutton value
            value = self.Settings.get_setting(SettingSections.OPTIONS, "keep_aspect_ratio")
            self.chk_keep_aspect_ratio = builder.get_object("chk_keep_aspect_ratio")
            self.chk_keep_aspect_ratio.set_active(value == True)

            # Save image type (combobox) value
            setting_value = self.Settings.get_setting(SettingSections.OPTIONS, "save_as_file_type",
                                                      default="JPG")
            self.combo_save_as_type = builder.get_object("combo_save_as_type")
            model = self.combo_save_as_type.get_model()
            for i, row in enumerate(model):
                store_value = row[:]
                if store_value[0].lower() == setting_value.lower():
                    self.combo_save_as_type.set_active(i)
                    break
                            
            
            # Condition section (Only resize image(s) if the dimensions are higher than...
            value = self.Settings.get_setting(SettingSections.OPTIONS, "resize_only_if_higher", False)
            self.chk_if_higher.set_active(value == True)
            self.spin_ifhigher_height.set_sensitive(value == True)
            self.spin_ifhigher_width.set_sensitive(value == True)
            # Load spinbutton values
            
            # Width
            value = self.Settings.get_setting(SettingSections.OPTIONS, "resize_only_if_width", 
                                              default=2048)
            self.spin_ifhigher_width.set_value(value)            
            
            # Height
            value = self.Settings.get_setting(SettingSections.OPTIONS, "resize_only_if_height", 
                                              default=1536)
            self.spin_ifhigher_height.set_value(value)
            
            
            
                            
            # JPG quality value from config
            value = self.Settings.get_setting(SettingSections.OPTIONS, "jpg_quality_percent",
                                              default=85)
            self.spin_jpg_quality = builder.get_object("spin_jpg_quality")
            self.spin_jpg_quality.set_value(value)
            
            
            # Set radiobutton value for single or multi processor setting
            value = self.Settings.get_setting(SettingSections.OPTIONS, "cpu_utilization")
            if value == "single":
                self.radiobutton_single_cpu.set_active(True)
            else:
                self.radiobutton_multi_cpu.set_active(True)
            
            
            # Load 'File Name' tab setting values
            value = self.Settings.get_setting(SettingSections.FILENAME, "ending_file_name",
                                              default="_resized")
            
            # If the resized text is returned as an int (it will occur if the text is just a number),
            # then convert it to a str
            if type(value) is int:
                value = str(value)
                
            if len(value) > 10:
                # Default to '_resized'
                value = "_resized"
            # Show resized file name in entry widget
            self.entry_resized_file_name.set_text(value)
            # If it's set to an empty string, update the 'example-label' to match the source image.
            if value == "":
                self.lbl_filename_example.set_label("test.jpg")
            
            # Load 'file exists decision' radio button value
            value = self.Settings.get_setting(SettingSections.FILENAME, "destination_exists_decision",
                                              default="unique")
            if value == "unique":
                self.radiobutton_save_as_unique.set_active(True)
            elif value == "skip":
                self.radiobutton_skip_image.set_active(True)
            elif value == "overwrite":
                self.radiobutton_overwrite_destination.set_active(True)
                
            
            # Get the cpu-count label
            self.lbl_cpu_count = builder.get_object("lbl_cpu_count")
            
            # Get the cpu count
            current_cpu_count = cpu_count()
            
            # Show the cpu count to the user

            # cpu_count() will return None if it's unable to get the cpu count.
            if not current_cpu_count:
                current_cpu_count = "Unknown"
                
            # Show the result to the user
            self.lbl_cpu_count.set_label(str(current_cpu_count))
            

        #test_msg = """This is just a test 
#to see what a multiline string looks like
#in a message dialog."""

        #MsgBox = MessageDialog(self.main_window, test_msg, None,
                     #MessageDialog.DialogType.QUESTION, "YOYO", MessageDialog.DialogButtons.OK_CANCEL)
        #response = MsgBox.show_dialog()

        #if response == MessageDialog.DialogResponse.OK:
            #MsgBox.primary_text = "Response was OK"
            #MsgBox.show_dialog()
        
        self.update_total_in_column_title()
    


    def get_config_file_path(self):
        """
        Get the expected default full path of where the settings file should be.
        The config file is also checked to see that it exists or not.
        
        If the file exists, the path to the config file is returned, otherwise None is returned.
        
        Linux:
        Copy the config file to $SNAP_USER_DATA, if the environmental variable exists.
        If the environmental variable, $SNAP_USER_DATA, doesn't exist, then we're not running in snap-mode.
        Note: example of where openresizer.py might be in, when in Snap mode: /snap/openresizer/x1/bin
        
        If the environmental variable, $SNAP_USER_DATA, is found - then the path to
        the config file in $SNAP_USER_DATA will be returned, otherwise a path to the config file 
        in the same directory of the script will be returned (if it's there).
        
        Windows:
        Check the Local App Data folder, and if the config file is not there, check for the config in the same
        directory of the script.
        """
        
        # with cx_Freeze, modules[__name__].__file__ will return an error:  module '__main__' has no attribute '__file__'
        # So if we're running in Windows, use relative paths.
        
        if DefaultSettings.PLATFORM == CurrentOS.RUNNING_LINUX:
            # In Linux, we need the full path (Snap's seem to prefer it, rather than relative paths)
            # We don't use this variable for Windows, it's used just in Linux.
            config_source_full_path = path.join(path.dirname(modules[__name__].__file__), "config")        

        
        
        # The config file name by itself
        config_file_name = "config"
        
        if DefaultSettings.PLATFORM == CurrentOS.RUNNING_LINUX:
            # In Linux, check to see if the script is running in a Snap. If so, the config
            # file needs to be copied to $SNAP_USER_DATA, and read/write from there, if it's not already
            # copied there.
            
            # Are we running in a Snap?
            if "SNAP_USER_DATA" in environ:
                # This script is running in a Snap
                
                # Get snap_user_data folder from environmental variable
                # This will be the folder where it's safe to write to.
                snap_user_data_folder = environ["SNAP_USER_DATA"]
                
                # Make destination path to config file in $SNAP_USER_DATA
                config_snap_user_data_full_path = path.join(snap_user_data_folder, config_file_name)
        
                # Debug purposes
                print(f"Debug: Snap source config file assumed: {config_source_full_path}")                
                
                # Copy the config file to the environmental variable path, in $SNAP_USER_DATA.
                # But only if the config file doesn't already exist.
                if not path.isfile(config_snap_user_data_full_path):
                    # The config file doesn't exist in $SNAP_USER_DATAs, copy it.
                    # The return value will the destination full path.
                    
                    try:
                        print(f"Debug: Attempting to copy from: {config_source_full_path} to {config_snap_user_data_full_path}")
                        
                        # Attempt to copy the source config file to a writable snap user-data location
                        return copyfile(config_source_full_path, config_snap_user_data_full_path)
                    
                    except OSError as e:
                        MsgBox = MessageDialog(self.main_window, "Could not copy config file.", f"Source: {config_full_path}\nDestination: {config_snap_user_data_full_path}", MessageDialog.DialogType.ERROR, "Copy Error")
                        response = MsgBox.show_dialog()
                                
                else:
                    # The config file already exists in $SNAP_USER_DATA, so return the path.
                    return config_snap_user_data_full_path
                
                
            else:
                # Debug purposes
                print(f"Debug: No snap environment found.") 
        
                
                #return config_full_path
    

        # If we're in Windows, check the local app data path
        elif DefaultSettings.PLATFORM == CurrentOS.RUNNING_WINDOWS:
            
            # Get the local app data path
            if "LOCALAPPDATA" in environ:
                local_app_data = environ["LOCALAPPDATA"]
                
                print(f"Debug: Local App Data path: {local_app_data}")
                
                # Get the full path to where the config file is expected in app data
                config_full_path = path.join(local_app_data, "OpenResizer", config_file_name)
                
                print(f"Debug: Local App Data path to config: {config_full_path}")
                
                # Is the config file in app data?
                if path.isfile(config_full_path):
                    return config_full_path
    
    
    
        # If we got here, that means the config file was not found in a Snap folder or wasn't found in the Local App Data folder.
        # The only other place to check now is the current folder where the script is running from, as a last resort.
        # This is now regardless of which OS the script is running.
        if path.isfile(config_file_name):
            return config_file_name
        else:
            return
            
    
    def update_total_in_column_title(self):
        """
        Update the treeview column's title, to reflect the count of items in the treeview.
        """
        
        # Get the treeview's model
        liststore = self.gtk_treeview.get_model()
        
        model_length = len(liststore)
        
        if model_length == 1:
            new_title = "Image Path (1 file)"
        else:
            new_title = "Image Path ({} files)".format(str(model_length))
        
        self.wrapper_treeview.set_column_title(0, new_title)
            

    def on_radiobutton_resize_type_changed(self, radio_button):
        # Get the name of the radio button that was selected
        #print(radio_button)
        widget_name = Gtk.Buildable.get_name(radio_button)
        
        if widget_name == "radiobutton_resize_by_pixel":

                # Set to 'Resize by pixel'
                self.frame_pixel.set_sensitive(True)
                self.frame_percentage.set_sensitive(False)
                
                # Enable the resize condition frame
                self.frame_condition.set_sensitive(True)                
        
        elif widget_name == "radiobutton_resize_by_percent":
                # Set to 'Resize by percentage'
                self.frame_pixel.set_sensitive(False)
                self.frame_percentage.set_sensitive(True)
                
                # Enable the resize condition frame
                self.frame_condition.set_sensitive(True)
        
        elif widget_name == "radiobutton_donotresize":
            
            # Disable 'Resize by pixel' and 'Resize by percentage' frames
            self.frame_pixel.set_sensitive(False)
            self.frame_percentage.set_sensitive(False)
            
            # Disable the resize condition frame
            self.frame_condition.set_sensitive(False)            
            

    def add_path_to_treeview(self):
        """Event handler for when the 'Add' button is clicked on the main window.
        
        Keyword arguments:
        button -- the button that was clicked
        """
        glade_file_path = get_glade_file_path(GladeWindowType.FILE_CHOOSER_DIALOG)

        # Initialize a file chooser object
        file_chooser = FileChooserBuilder(glade_file_path, self.main_window, 
                                         "Add images to resize list...", 
                                         "Add selected image(s)")
        
        
        file_chooser.new_filter_set(filter_label="Supported Images (*.jpg, *.png, *.bmp)", 
                                    file_patterns="*.jpg;*.png;*.bmp", 
                                    mime_types="image/jpeg;image/png;image/bmp")        
        
        file_chooser.new_filter_set(filter_label="Jpg Images", 
                                    file_patterns="*.jpg", 
                                    mime_types="image/jpeg")
        
        file_chooser.new_filter_set(filter_label="Png Images", 
                                    file_patterns="*.png", 
                                    mime_types="image/png")
        
        file_chooser.new_filter_set(filter_label="Bmp Images", 
                                    file_patterns="*.bmp", 
                                    mime_types="image/bmp")
        
        
        # Set the initial path, based on the last path that was recorded
        # but only if the folder still exists
        if self.initial_add_folder and path.isdir(self.initial_add_folder):
            file_chooser.file_chooser1.set_current_folder(self.initial_add_folder)
                
        # Show the dialog and get the Gtk.ResponseType return value
        response = file_chooser.show_dialog()      


        # Was the 'Open' button clicked? Get the file name paths.
        if response == Gtk.ResponseType.OK:
            # Open was clicked
            
            
            # Record the path that the file(s) were added from.
            # This gets saved to the config/settings file upon exit.
            self.initial_add_folder = file_chooser.file_chooser1.get_current_folder()            
            
            
            # Get the model of the treeview widget
            liststore = self.gtk_treeview.get_model()
            
            # Add the selected file(s) to the model, which automatically updates the treeview
            for file_path in file_chooser.file_chooser1.get_filenames():
                
                # Make sure the path isn't already in the treeview
                if not self.wrapper_treeview.row_exists(file_path, 0):
                    # The path doesn't already exist
                
                    # Get the file size in bytes
                    file_size = path.getsize(file_path)
                    
                    # Convert the bytes size to a human-readable format
                    human_size = humansize(file_size)
                    
                    # Add the path and human-file-size to the model, which automatically updates the treeview
                    liststore.append([file_path, human_size])
                
                        

        file_chooser.file_chooser1.destroy()
        
        # Show the new total in the column title)
        self.update_total_in_column_title()        
    
    
    def on_chk_if_higher_toggled(self, check_button):
        # Whent he checkbutton 'Only resize image(s) if the dimensions are higher than' is toggled.
        
        value = check_button.get_active()
        
        # Checked? If so, enable the spin buttons, otherwise, disable them.
        self.spin_ifhigher_width.set_sensitive(value == True)
        self.spin_ifhigher_height.set_sensitive(value == True)
        
    def on_entry_resized_file_name_changed(self, entry):
        # When the 'Ending of resized file name' entry changes in the 'File Name' tab.
    
        text = entry.get_text()
        example = "test" + text + ".jpg"
        
        self.lbl_filename_example.set_label(example)
        
    def on_mnu_quick_guide_activate(self, menu):
        # Quick guide clicked
        
        # Show the quick guide
        builder = Gtk.Builder()
        builder.add_from_file(get_glade_file_path(GladeWindowType.QUICK_GUIDE))
        
        builder.connect_signals(self)
        
        # Get the Quick-guide window
        quick_guide_window = builder.get_object("quick_guide_window")
        quick_guide_window.set_transient_for(self.main_window)
        
        # Show window
        quick_guide_window.show()
        
        
    def on_quick_guide_window_destroy(self, window):
        window.destroy()

        
    def on_mnu_about_activate(self, menu):
        # About menu clicked
        
        # Show the about window
        
        builder = Gtk.Builder()
        builder.add_from_file(get_glade_file_path(GladeWindowType.ABOUT_WINDOW))
        
        builder.connect_signals(self)
        
        # Get the about window
        about_window = builder.get_object("about_window")
        about_window.set_transient_for(self.main_window)
        response = about_window.run()
        
        if response == Gtk.ResponseType.DELETE_EVENT:
            about_window.destroy()
                    
    
    """
    Open the website link in the about window when it's clicked.
    """
    def on_about_window_activate_link(self, about_dialog, website_url):
        # When the website link for OpenResizer is clicked.
        web_open(website_url)
        
        # Return True so that the GtkLinkButton knows it's been handled.
        # That way, GtkLinkButton won't try to open the link itself.
        # Attempting to open a link in Windows seems to fail when using GtkLinkButton,
        # So that's why we handle it manually here.
        return True
    
    def on_btn_add_clicked(self, button):
        # When the 'Add' menu or button is clicked/activated
        self.add_path_to_treeview()
        
        
    def on_drag_and_drop(self, treeview_widget, drag_context, x, y, selection_data, info, timestamp):
        
        # The selection data is normally returned as a byte, so we use .decode() to convert it to a string
        data:str = selection_data.get_data().decode()
        
        # Remove all new line characters. If we don't do this, we'll have double-space lines.
        # \x00 (NULL) check is needed for Windows. Otherwise, we would get \x00 (NULL) at the end.
        data = data.strip("\n\r\x00")        
        
        # The full paths start with file://
        # Example:
        # file:///home/user_name/Desktop/ticotico.jpg

        # Split the single-string by a new line character
        path_list = data.splitlines() #data.split("\n")
                
        # This will hold the path to files without the 'file://' part
        final_list = []
                
        # Strip the file:// part
        for full_path in path_list:
            if DefaultSettings.PLATFORM == CurrentOS.RUNNING_WINDOWS and full_path.startswith("file:///"):
                # Windows
                
                # Get the path without file:// and remove any extra \r characters
                full_path = full_path[8:].strip()   
                                
                # Replaces forward slashes with back slashes
                # Example: C:/folder/file.jpg changes to C:\folder\file.jpg
                full_path = str(Path(full_path)) #full_path.replace(r"/", r"\")

            elif DefaultSettings.PLATFORM == CurrentOS.RUNNING_LINUX and full_path.startswith("file://"):
                # Linux
                
                # Get the path without file:// and remove any extra \r characters
                full_path = full_path[7:].strip()   
                      
            
            # Unquote the string (replace %20 with a space, etc)
            
            # During testing, before a drag and drop, 
            # the file name was called: 'Screenshot at 2018-07-29 16-28-16.png'
            # But after dragging and dropping, 
            # it got stored in a list as: 'Screenshot%20at%202018-07-29%2016-28-16.png'
            
            # So with the %20 symbols, functions such as path.isfile() were not finding the file.
            # So we had to impliment unquote(), which is from the 'urllib' module.
            
            # It was affecting folders and file names with spaces.
            # Unquote the string (replace %20 with a space, etc)
            full_path = unquote(full_path)
                                       
            # Is the full path just a folder, not a file? Get a list of files in the folder
            if path.isdir(full_path):
                # It's a folder, so iterate through it, including the sub directories.
                
                file_list = glob.glob(full_path + "/**/*.*", recursive=True)
                
                if len(file_list) > 0:
                    final_list.extend(file_list)
            
            else:
                # It's not a folder, it's a file.
                
                # Record the new normal full path to a file
                final_list.append(full_path)
        
            
                
        # Add to treeview
        for file_path in final_list:

            # Make sure the extension is a supported type
            if file_path[-4:].lower() in self.supported_formats:
                
                # Make sure the path isn't already in the treeview
                if not self.wrapper_treeview.row_exists(file_path, 0):
                
                    # Make sure the file exists
                    if path.isfile(file_path):
                        # Get the file size
                        file_size = path.getsize(file_path)
                        readable_size = humansize(file_size)
                        
                        # Add full path to treeview
                        self.wrapper_treeview.add_row(file_path, readable_size)
        
                        
        # Show the new total in the column title)
        self.update_total_in_column_title()
        
    def on_file_menu_activated(self, *args):
        """Event when the 'File' menu is clicked, from the top of the window.
        
        In this case, we want to show the 'Quit' menu and a seperator.
        We want to set its visibility to True, because a right-click on the TreeView widget
        might have hid the two menus.
        """
        self.menu_seperator1.set_visible(True)
        self.menu_quit.set_visible(True)
    
    
    def on_mouse_click_gtk_treeview(self, treeview, event):
        """Event for when a mouse button has been clicked on the TreeView widget.
        
        This will be used for detecting right-clicks to bring up a context menu.
        """
        if event.button == 3:
            # A right-click was initiated

            # Hide the seperator and Quit menu, because it's going to be a context menu.
            self.menu_seperator1.set_visible(False)
            self.menu_quit.set_visible(False)
            
            # Show the context menu
            self.mnu_file.popup(None, None, None, None, event.button, event.time)
         
            # The event has been handled.
            return True
        
    def on_btn_clear_list_clicked(self, button):
        """Event: the 'Clear List' button was clicked"""
        
        # Clear the liststore, which automatically clears the treeview      
        self.wrapper_treeview.clear()
        
        # Show the new total in the column title)
        self.update_total_in_column_title()        
            
    def on_menu_add_activate(self, *args):
        self.add_path_to_treeview()
        
    def on_menu_remove_activate(self, *args):
        self.remove_selected_path()
        
    def remove_selected_path(self):
        self.wrapper_treeview.delete_selected_rows()
        
        # Show the new total in the column title)
        self.update_total_in_column_title()        
        
    def on_treeview_keypress(self, treeview_object, event_key):
        # Using Accelerator key instead of this, because then
        # it will show the <delete> shortcut in the File menu and context menu.
        pass
    
    
        ## Get the numeric value of the key that was pressed
        #keyval = event_key.keyval
        
        #if keyval == Gdk.KEY_Delete:
            #self.wrapper_treeview.delete_selected_rows()
                        
            ## Show the new total in the column title)
            #self.update_total_in_column_title()                    


    def on_menu_preview_activate(self, *args):
        
        # Get the selected path(s) in the treeview, if any, as a list.
        selected_paths = self.wrapper_treeview.get_selected_row_values(0)
        
        # Make sure there is at least one selection.
        # However, later below we will only be looking at the first selection, even if more are selected.
        if selected_paths:
            
            progress_glade_path = get_glade_file_path(GladeWindowType.PROGRESS_WINDOW)
            
            # Load and show the progress window.
            self.progress_window = ProgressWindow(progress_glade_path, self.main_window, 1, None)
            self.progress_window.show_window()
            # self.show_progress_window()
            
            
            # Create a temp folder for the temporary resized image file
            config.preview_temp_dir = TemporaryDirectory()
            
            # Make sure the temp folder exists
            if not path.isdir(config.preview_temp_dir.name):
                message = MessageDialog(window.main_window, "Temp folder not found.", 
                                        config.preview_temp_dir.name, 
                                        MessageDialog.DialogType.ERROR, "Temp Folder Not Found", 
                                        MessageDialog.DialogButtons.OK)
                
                response = message.show_dialog()
                
                return            

            t = Thread(target=self.begin_resize, args=(config.preview_temp_dir.name, selected_paths[0]))
            t.daemon = True
            t.start()
            
    
    def on_window_delete_event(self, *args):
        """
        This signal handler is called before the window is destroyed.
        
        It's used for showing the user a 'Are you sure you want to exit?' type dialog,
        but in our case we use it to get the window's size, because this is where
        the user-adjusted size gives a proper value. If we try to get the size of the window
        from on_window_destroy(), it'll give us the default window size.
        """
        self.adjusted_width, self.adjusted_height = self.main_window.get_size()

            

    def on_window_destroy(self, *args):
        
        # Update settings dictionary from GUI widget values        
        self.refresh_settings_dictionary()
        
        # Save all dictionary settings to file before closing the program
        self.Settings.save_to_file()
               
        # Quit the main loop
        Gtk.main_quit()
        

        
    def refresh_settings_dictionary(self):
        # Updates the values in the class's settings dictionary to reflect
        # the values in the GUI widgets.
        
        # This method is run before the program has quit. After this method, the refreshed-dictionary
        # will then be saved to a config file, so the settings of the app get saved.
        
        
        
        # Save the last 'add_folder' and 'save_folder' paths.
        # They're not really GUI widget values, but we need to save their values.
        self.Settings.save_setting(SettingSections.GENERAL, "initial_add_folder", self.initial_add_folder)
        self.Settings.save_setting(SettingSections.GENERAL, "initial_save_folder", self.initial_save_folder)
        
        
        # Get the selected image-type (combobox) index
        selected_index = self.combo_save_as_type.get_active()
        # Get the model of the combobox, so we can get the text of the index
        model = self.combo_save_as_type.get_model()
        read_selected_image_type = model[selected_index][0]
                
        # Update settings dictionary
        self.Settings.save_setting(SettingSections.OPTIONS, "save_as_file_type", read_selected_image_type)
        
        # Get JPG quality value from GUI (the spinner widget returns a float-type)
        read_jpg_quality_value = int(self.spin_jpg_quality.get_value())
        # Update settings dictionary
        self.Settings.save_setting(SettingSections.OPTIONS, "jpg_quality_percent",
                                   read_jpg_quality_value)
        
        
        # Get radio button ('Do not resize'). get_active() returns a bool value
        is_toggled = self.radiobutton_donotresize.get_active()
        if is_toggled:
            self.Settings.save_setting(SettingSections.OPTIONS, "resize_type", "noresize")
        
        else:
            # Get radio button value (Resize by pixel). get_active() returns a bool value
            is_toggled = self.radiobutton_resize_by_pixel.get_active()
            if is_toggled:
                # Update settings dictionary
                self.Settings.save_setting(SettingSections.OPTIONS, "resize_type", "pixel")
            else:
                # Update settings dictionary
                self.Settings.save_setting(SettingSections.OPTIONS, "resize_type", "percent")
            
        
        
        # Get spinner value for 'Resize to width (pixels)
        spinner_value = int(self.spin_resize_width.get_value())
        # Update settings dictionary
        self.Settings.save_setting(SettingSections.OPTIONS, "resize_to_width", spinner_value)
        
                
        # Get spinner value for 'Resize to height (pixels)
        spinner_value = int(self.spin_resize_height.get_value())
        # Update settings dictionary
        self.Settings.save_setting(SettingSections.OPTIONS, "resize_to_height", spinner_value)
        
                
        # Get spinner value for 'Decrease dimensions by (percent)
        spinner_value = int(self.spin_decrease_size_percent.get_value())
        # Update settings dictionary
        self.Settings.save_setting(SettingSections.OPTIONS, "decrease_size_by", spinner_value)        
        
        
        
        # Get 'Keep Aspect Ratio' checkbutton value. get_active() returns a bool
        keep_aspect_ratio = self.chk_keep_aspect_ratio.get_active()
        # Update settings dictionary
        self.Settings.save_setting(SettingSections.OPTIONS, "keep_aspect_ratio", keep_aspect_ratio)
        
        
        # Get 'Resize only if dimensions are higher than ...' settings.
        
        # Returns a bool
        resize_only_if_higher = self.chk_if_higher.get_active()
        only_if_higher_width = int(self.spin_ifhigher_width.get_value())
        only_if_higher_height = int(self.spin_ifhigher_height.get_value())
        # Update settings dictionary
        self.Settings.save_setting(SettingSections.OPTIONS, "resize_only_if_higher", resize_only_if_higher)
        self.Settings.save_setting(SettingSections.OPTIONS, "resize_only_if_width", only_if_higher_width)
        self.Settings.save_setting(SettingSections.OPTIONS, "resize_only_if_height", only_if_higher_height)
        
        
        
        # Get 'CPU utilization' radiobutton value. get_active() returns a bool
        if self.radiobutton_multi_cpu.get_active():
            cpu_usage = "multi"
        else:
            cpu_usage = "single"
        self.Settings.save_setting(SettingSections.OPTIONS, "cpu_utilization", cpu_usage)
        
        
        
        # File name tab
        
        # Save resized file name
        resized_file_name = self.entry_resized_file_name.get_text()
        self.Settings.save_setting(SettingSections.FILENAME, "ending_file_name", resized_file_name)
        
        # Overwrite radio button decision
        if self.radiobutton_save_as_unique.get_active():
            value = "unique"
        elif self.radiobutton_skip_image.get_active():
            value = "skip"
        elif self.radiobutton_overwrite_destination.get_active():
            value="overwrite"
        self.Settings.save_setting(SettingSections.FILENAME, "destination_exists_decision", value)
        
        
        # Save window size maximize/normal state
        window_maximized = self.main_window.is_maximized()
        if window_maximized:
            value = "maximized"
        else:
            value = "normal"
        self.Settings.save_setting(SettingSections.MAINWINDOW, "state", value)
        
        
        # Save window size in pixels (width/height), if we have the values
        
        # The only time we would have the values, is when the main window is being closed, in the delete_event handler.
        # If this method is being run because of a resize request, then we won't have a adjusted_width or adjusted_height.
        if self.adjusted_width and self.adjusted_height:
            self.Settings.save_setting(SettingSections.MAINWINDOW, "width", self.adjusted_width)
            self.Settings.save_setting(SettingSections.MAINWINDOW, "height", self.adjusted_height)
            
            
        
        #model = self.combo_save_as_type.get_model()
        #for i, row in enumerate(model):
            #store_value = row[:]
            #if store_value[0].lower() == setting_value.lower():
                #self.combo_save_as_type.set_active(i)
                #break        


    def show_progress_window(self):
        
        # Instantiate builder, so we can load the progress UI
        builder = Gtk.Builder()
        builder.add_from_file(get_glade_file_path(GladeWindowType.PROGRESS_WINDOW))
        
        # Get the progress window
        progress_window = builder.get_object("progress_window")
        
        # Get the progress bar
        self.progress_bar = builder.get_object("progress_bar")
        
        # Get the scrolled-window, so we can autoscroll it to the bottom
        # used by the on_auto_scroll signal handler.
        self.progress_scrolled_window = builder.get_object("progress_scrolled_window")
                
        # Set the trasient window, so the main window can't be interacted with
        # while the progress window is being displayed.
        progress_window.set_transient_for(self.main_window)
        
               
        # Get the textview
        txt_log = builder.get_object("txt_log")
        
        # Get the textbuffer of the textview
        self.progress_log_text_buffer = txt_log.get_buffer()
        
        
        
        # Show the progress window
        progress_window.show_all() 
        


    def on_save_to_folder_clicked(self, button):
        
        # First, make sure there are source image files in the treeview model.
        if self.wrapper_treeview.get_count() == 0:
            message = MessageDialog(self.main_window, "No images specified.",
                                    "Please add at least one source image to the list.", 
                                    MessageDialog.DialogType.ERROR, "No images specified")
            response = message.show_dialog()
            
            return
        
        
            
        glade_file_path = get_glade_file_path(GladeWindowType.FOLDER_CHOOSER_DIALOG)
        folder_chooser = FolderChooserBuilder(glade_file_path, self.main_window, 
                                              "Select Destination Folder", "Start Resizing",
                                        "Choose a destination folder, then click 'Start Resizing' to begin.")
        
        # Set the initial folder, if there is one
        if self.initial_save_folder and path.isdir(self.initial_save_folder):
            folder_chooser.file_chooser1.set_current_folder(self.initial_save_folder)
        
        
        result = folder_chooser.show_dialog()
        if result == Gtk.ResponseType.OK:
            destination_path = folder_chooser.file_chooser1.get_current_folder()
            
            folder_chooser.file_chooser1.hide()
            
            progress_glade_path = get_glade_file_path(GladeWindowType.PROGRESS_WINDOW)
            
            # Get the total count of the number of images to be processed.
            # This is used for the 'Total Files:' label in the progress window.
            total_images = self.wrapper_treeview.get_count()
            
            # Load and show the progress window.
            self.progress_window = ProgressWindow(progress_glade_path, self.main_window, total_images, destination_path)
            self.progress_window.show_window()
            # self.show_progress_window()
            
            # Save the destination path, so that the file chooser dialog will start from there next time.
            self.Settings.save_setting(SettingSections.GENERAL, "initial_save_folder", destination_path)
            
            # Save the destination path to a variable, so it can get saved to the settings file
            # once the main window closes.
            self.initial_save_folder = destination_path
            
            
            # Start a new thread, to begin the resize process.
            
            # We use a separate thread so that the GUI can stay responsive during the resize process.
            # That way, the user can cancel long-running resizes if requested.
            
            t = Thread(target=self.begin_resize, args=(destination_path,))
            t.daemon = True
            t.start()

            #self.begin_resize(destination_path)
                
        
        folder_chooser.file_chooser1.destroy()
        
               
        
    
    def begin_resize(self, destination_path, preview_source_image_path=None):
        
        # The multiprocessing function 'imap_unordered' can only take one argument.
        # So we need to get all the options the user has chosen and record them into variables.
        # Then we need to make a partial function, which gets passed to the main resize method
        # which uses the multiprocessing module.
                                
        
        # The first step in getting the values of the options that the user has chosen
        # is to refresh the settings dictionary based on the values of the GUI widgets.
        
        # Refresh the settings dictionary based on the GUI options
        self.refresh_settings_dictionary()
                
        # Now we can set individual variables based on the settings dictionary.
        # At the end, we'll combine all the settings the user has chosen and put them into
        # a single namedtuple, ready for the multiprocessing method of 'imap_unordered'.


        def get_preview_resized_path(preview_response):
            """
            Take a successful preview response (such as: Preview OK - /home/file_resized.jpg)
            and return just the path to the resized image, such as: /home/file_resized.jpg)
            
            This method is used only during the process of previewing a resized image.
            
            Keyword arguments:
            preview_response -- the successful preview response, which should be like: Preview OK - {full path to resized image}
            """
            final_return = None
            
            if preview_response:
                first_dash_loc = preview_response.find("-")
                
                if first_dash_loc != -1:
                    preview_response = preview_response[first_dash_loc + 2:]
                    
                    if preview_response:
                        final_return = preview_response
        
                
            return final_return        


        
        # Make sure the destination path exists.
        if not path.isdir(destination_path):
            
            # Show the error in the progress window
            GLib.idle_add(self.progress_window.show_error_message, "Destination path not found.", "Error")
            
            return False
        
         
        # Initialize
        resize_instructions = None
        
        # Record setting for - resize_instructions
        value = self.Settings.get_setting(SettingSections.OPTIONS, "resize_type")
        
        if value == "noresize":
            resize_instructions = ResizeInstructions.DO_NOT_RESIZE
            
        elif value == "percent":
            resize_instructions = ResizeInstructions.DECREASE_PERCENT
        
        elif value == "pixel":
            
            # Keep the aspect ratio?
            value = self.Settings.get_setting(SettingSections.OPTIONS, "keep_aspect_ratio")
            if value:
                resize_instructions = ResizeInstructions.SPECIFIC_DIMENSIONS_KEEP_ASPECT
            else:
                resize_instructions = ResizeInstructions.SPECIFIC_DIMENSIONS
            
        
        
        # No resize type set? Show an error and don't allow the resize to start.
        if not resize_instructions:

            # Show the error in the progress window
            GLib.idle_add(self.progress_window.show_error_message, "Resize type option could not be read.",
                          "Error")            
            
            return False
        
        
        # Initialize
        save_as_format = None
        
        # Record setting for - save_as_format
        value = self.Settings.get_setting(SettingSections.OPTIONS, "save_as_file_type")
        
        if value == "Keep Original":
            save_as_format = SaveAsExtension.SOURCE_EXTENSION
        elif value:
            # Find the extension in the SaveAsExtension enum
            for extension in SaveAsExtension:
                # extension is the enum
                if extension.name == value:
                    
                    # We found the enum for the extension that the user wants to save in.
                    save_as_format = extension
                    break
    
        
        # No save as type set? Show an error and don't allow the resize to start.
        if not save_as_format:
            
            # Show the error in the progress window
            GLib.idle_add(self.progress_window.show_error_message, "Save as format option could not be read.",
                          "Error")               
            
            return False
    
    
    
        
        # Initialize
        destination_size_info = None
    
    
        # depending on whether a fixed size has been chosen by the user or a percentage decrease.
        if resize_instructions == ResizeInstructions.DECREASE_PERCENT:
            # Decrease by percentage
            
            # Get the value selected in the GUI
            value = self.Settings.get_setting(SettingSections.OPTIONS, "decrease_size_by")
            
            # Record the value, so we can send it to the main resize method later through
            # a named tuple.
            destination_size_info = value
        
        elif resize_instructions == ResizeInstructions.SPECIFIC_DIMENSIONS or resize_instructions == ResizeInstructions.SPECIFIC_DIMENSIONS_KEEP_ASPECT:
            # Fixed width/height
            value = self.Settings.get_setting(SettingSections.OPTIONS, "resize_to_width")
            value_height = self.Settings.get_setting(SettingSections.OPTIONS, "resize_to_height")
        
            # If we have a width and height, record them as a tuple.
            if value and value_height:
                destination_size_info = (value, value_height)
        
        elif resize_instructions == ResizeInstructions.DO_NOT_RESIZE:
            # Don't resize
            destination_size_info = None

        ## No destination size info? Show an error and don't allow the resize to start.
        #if not destination_size_info:
            
            ## Show the error in the progress window
            #GLib.idle_add(self.progress_window.show_error_message, "Destination size info could not be read.",
                          #"Error")               
            
            #return False
        
        
        # Record setting for cpu usage
        value = self.Settings.get_setting(SettingSections.OPTIONS, "cpu_utilization", "multi")
        if value == "single":
            cpu_usage = ProcessorUsage.SINGLE_CPU
        else:
            cpu_usage = ProcessorUsage.MULTI_CPU
        
        
        # Intialize
        append_resized_file_name = ""
        
        # Record setting for - append_resized_file_name
        value = self.Settings.get_setting(SettingSections.FILENAME, "ending_file_name", "")
        append_resized_file_name = value
        
        
        
        # Record setting for - quality
        value = self.Settings.get_setting(SettingSections.OPTIONS, "jpg_quality_percent")
        quality = value
        
        
        
        # Initialize
        file_exists_decision = None
        
        # Record setting for - file_exists_decision
        value = self.Settings.get_setting(SettingSections.FILENAME, "destination_exists_decision")
        if value == "unique":
            file_exists_decision = FileExistsDecision.UNIQUE_FILENAME
        elif value == "skip":
            file_exists_decision = FileExistsDecision.SKIP_FILE
        elif value == "overwrite":
            file_exists_decision = FileExistsDecision.OVERWRITE_FILE
        
        

        # No overwrite decision info? Show an error and don't allow the resize to start.
        if not file_exists_decision:

            # Show the error in the progress window
            GLib.idle_add(self.progress_window.show_error_message, 
                          "File exists decision setting could not be read.", "Error")               
            
            return False
    
    
        # Get condition - only resize image(s) if the dimensions are higher than...
        value = self.Settings.get_setting(SettingSections.OPTIONS, "resize_only_if_higher")
        if value:
            resize_condition = ResizeCondition.DIMENSIONS_HIGHER_THAN

        else:
            resize_condition = ResizeCondition.NO_CONDITION
        
        # Get the condition width/height, regardless of whether we need to use it or not,
        # because the main resize method will expect it.
        only_if_higher_width = self.Settings.get_setting(SettingSections.OPTIONS, "resize_only_if_width")
        only_if_higher_height = self.Settings.get_setting(SettingSections.OPTIONS, "resize_only_if_height")
        
        if not only_if_higher_width or not only_if_higher_height:
            # Show the error in the progress window
            GLib.idle_add(self.progress_window.show_error_message, 
                          "Condition decision setting could not be read.", "Error")               
            
            return False            
        
        # Record tuple of width/height (the resize method will expect this, whether we're using this
        # option or not).
        only_if_higher_dimensions = (only_if_higher_width, only_if_higher_height)
        
        
        # If a preview source image has been specified, then enable preview_mode.
        # preview_mode will cause a successful image resize to return as:
        # Preview OK - (full path to resized image)
        # rather than just OK - (file name to resized image).
        # That way, we can load the resized image in a preview window and delete the file after it's loaded.
        if preview_source_image_path:
            preview_mode = True
        else:
            preview_mode = False
        
        
        # Get the model of the treeview, which holds the data
        liststore = self.gtk_treeview.get_model()
        
        # Make partial method. This will cause all parameters in the resize_image method be static,
        # except for the first argument, which will be the source image path.
        
        # We create a partial method because imap_unordered can only accept one iterable argument.
        partial_resize_method = partial(resize_image,
                                 resize_instructions=resize_instructions,
                                 resize_condition=resize_condition,
                                 only_if_higher_dimensions=only_if_higher_dimensions,
                                 save_as_format=save_as_format,
                                 destination_size_info=destination_size_info,
                                 destination_folder_path=destination_path,
                                 append_resized_file_name=append_resized_file_name,
                                 quality=quality,
                                 file_exists_decision=file_exists_decision,
                                 preview_mode=preview_mode)        
        
        
        #print(destination_path)
        
        source_images = []
        
        
        # Has a preview source image path been specified? If so, this is only for preview purposes.
        if preview_source_image_path:
            # Add the preview image to the resize list.
            source_images.append(preview_source_image_path)
        
        else:
            # We're going to resize the images in the liststore, it's not a preview.
            
            for row in liststore:
                # Get the path to the source image that we want to resize
                # 0 means the first column
                current_path = row[0]
    
                # Add source full path to list
                source_images.append(current_path)
        
        # print(source_images)
        

        
        
        # Create pool, which gets used for multiprocessing during resizing.
        
        # Are we creating a single process pool? (single processor setting)
        if cpu_usage == ProcessorUsage.SINGLE_CPU:
            #pool = Pool(1)
            num_process = 1
        else:
            # Multi processing mode.
            # When Pool() has no parameter specified (None), the number of processes will match
            # the number of cpu counts returned by os.cpu_count()
            num_process = None
            #pool = Pool()
        
        # Initialize variables used for showing the progress
        progress_so_far = 0
        success_count = 0
        warning_count = 0
        error_count = 0        
        total_count = len(source_images)
        preview_full_path = None
        
        # Record the start time
        start_time = time()
        
        with Pool(processes=num_process) as pool:
        
            # Multiprocess
            for result in pool.imap_unordered(partial_resize_method, source_images):
                # print(result)
                progress_so_far += 1
                
                # If the result starts with 'OK', then consider it a success.
                if len(result) >= 2:
                    if result[0:2] == "OK":
                        success_count += 1
                        
                    elif preview_mode and result.startswith("Preview OK -"):
                        success_count +=1
                        
                        # Get the full path to the resized preview image
                        preview_full_path = get_preview_resized_path(result)
                        
                        
                    elif result.startswith("Warning"):
                        warning_count += 1
                        
                    elif result.startswith("Error"):
                        error_count += 1
                else:
                    error_count +=1
                
                # Update progress window
                GLib.idle_add(self.progress_window.update_progress, progress_so_far, total_count, 
                              success_count, error_count, warning_count, result)
                
                # Has the user cancelled the progress?
                if self.progress_window.cancel_process:
    
                    # The user wants to cancel, so close the progress window
                    GLib.idle_add(self.progress_window.close_progress_window)
                
                    # Exit the for-loop, because the user has cancelled
                    break
                
                    # Terminate the pool
                    #pool.terminate()  (No need any more, since we're using a 'with' statement)
                
        
        # Not needed anymore, since we're using 'with' for the pool.       
        #pool.close()
        #pool.join()
        
        # Record the end time.
        end_time = time()
    
        # multiprocessing has finished.
        # If it wasn't cancelled, show the user that the resize process has completed.
        if not self.progress_window.cancel_process:
            GLib.idle_add(self.progress_window.progress_finished, start_time, 
                          end_time, destination_path, preview_source_image_path, 
                          preview_full_path)
        else:
            # if we got here, that means the user has cancelled, but the progress window is still open.
            # This may happen if the last image finishes resizing, but the cancel button is clicked shortly after by the user.
            
            # After the last image has been resized, there is a very slight delay before the 'Cancel' button turns to an 'OK' button.
            # In that very short delay, if the user clicks on the cancel button, there won't be anything to cancel (since all the images
            # have already been resized, so it'll just stay there in a 'Cancelling state'. This seems to be a race condition.
            
            # so exit the window here, to avoid the user from just seeing 'Cancelling...'.
            self.progress_window.close_progress_window()


if __name__ == "__main__":
    
    # Required for cx_Freeze
    freeze_support()
    
    # Get the OS that we're on
    current_os = osname()
    if current_os == "Linux":
        DefaultSettings.PLATFORM = CurrentOS.RUNNING_LINUX
    elif current_os == "Windows":
        DefaultSettings.PLATFORM = CurrentOS.RUNNING_WINDOWS
    
    # Create the main window
    window = MainWindow()


    # Run the main Gtk loop
    Gtk.main()
