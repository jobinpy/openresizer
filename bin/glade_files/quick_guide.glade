<?xml version="1.0" encoding="UTF-8"?>
<!-- Generated with glade 3.22.0 -->
<interface>
  <requires lib="gtk+" version="3.20"/>
  <object class="GtkWindow" id="quick_guide_window">
    <property name="width_request">512</property>
    <property name="height_request">400</property>
    <property name="can_focus">False</property>
    <property name="title" translatable="yes">Quick guide - OpenResizer</property>
    <property name="window_position">center-always</property>
    <property name="default_width">440</property>
    <property name="default_height">250</property>
    <property name="destroy_with_parent">True</property>
    <property name="icon_name">system-help</property>
    <signal name="destroy" handler="on_quick_guide_window_destroy" swapped="no"/>
    <child>
      <object class="GtkBox">
        <property name="visible">True</property>
        <property name="can_focus">False</property>
        <property name="orientation">vertical</property>
        <child>
          <object class="GtkStackSwitcher" id="main_switcher">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="margin_left">2</property>
            <property name="margin_top">5</property>
            <property name="stack">main_stack</property>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">0</property>
          </packing>
        </child>
        <child>
          <object class="GtkStack" id="main_stack">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="transition_type">crossfade</property>
            <child>
              <object class="GtkBox" id="box_welcome">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="margin_left">5</property>
                <property name="margin_right">5</property>
                <property name="margin_top">5</property>
                <property name="orientation">vertical</property>
                <child>
                  <object class="GtkLabel">
                    <property name="visible">True</property>
                    <property name="can_focus">False</property>
                    <property name="halign">start</property>
                    <property name="label" translatable="yes">&lt;span font='11'&gt;&lt;big&gt;&lt;b&gt;Welcome to OpenResizer&lt;/b&gt;&lt;/big&gt;
OpenResizer is an open-source bulk image resizing software. It is designed to be fast and easy-to-use.

OpenResizer supports parallel processing - which, hardware-permitting, enables multiple images to be resized at the same time.

This quick guide will explain the main features to help get you started.&lt;/span&gt;</property>
                    <property name="use_markup">True</property>
                    <property name="wrap">True</property>
                    <property name="selectable">True</property>
                  </object>
                  <packing>
                    <property name="expand">False</property>
                    <property name="fill">True</property>
                    <property name="position">0</property>
                  </packing>
                </child>
              </object>
              <packing>
                <property name="name">page0</property>
                <property name="title" translatable="yes">Welcome</property>
              </packing>
            </child>
            <child>
              <object class="GtkBox" id="box_quick_steps">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="margin_left">5</property>
                <property name="margin_right">5</property>
                <property name="margin_top">5</property>
                <property name="orientation">vertical</property>
                <child>
                  <object class="GtkScrolledWindow">
                    <property name="visible">True</property>
                    <property name="can_focus">True</property>
                    <child>
                      <object class="GtkViewport">
                        <property name="visible">True</property>
                        <property name="can_focus">False</property>
                        <property name="shadow_type">none</property>
                        <child>
                          <object class="GtkLabel">
                            <property name="visible">True</property>
                            <property name="can_focus">False</property>
                            <property name="halign">start</property>
                            <property name="valign">start</property>
                            <property name="label" translatable="yes">&lt;span font='11'&gt;&lt;b&gt;&lt;big&gt;Quick Steps&lt;/big&gt;&lt;/b&gt;

&lt;b&gt;1. Add Source Images&lt;/b&gt;
Add the images that you want to resize. This can be done by clicking the 'Add' button, or by dragging and dropping from your operating system's file manager. You can also drag and drop folders.

&lt;b&gt;2. Choose 'Save As Type' image format.&lt;/b&gt;
From the top of the main window, choose the image format that you want to save your images as, such as JPG, PNG or BMP.

&lt;b&gt;3. Set Resize Options&lt;/b&gt;
Click the 'Options' tab and choose your resize options.

&lt;b&gt;4. Set Destination File Name&lt;/b&gt;
Click the 'File Name' tab to specify what the ending of resized file names should be, and to also decide what should happen if a destination file already exists.

&lt;b&gt;5. See a Preview&lt;/b&gt;
To see a before-and-after preview, right-click on any image you want on the list, and then click 'Preview'.

&lt;i&gt;&lt;b&gt;And Finally...&lt;/b&gt;&lt;/i&gt;
Once you're happy with the preview, click '&lt;u&gt;Save to Folder...&lt;/u&gt;' (at the top-right of the main window) to select a destination folder and begin the resize process.&lt;/span&gt;</property>
                            <property name="use_markup">True</property>
                            <property name="wrap">True</property>
                            <property name="selectable">True</property>
                          </object>
                        </child>
                      </object>
                    </child>
                  </object>
                  <packing>
                    <property name="expand">True</property>
                    <property name="fill">True</property>
                    <property name="position">0</property>
                  </packing>
                </child>
              </object>
              <packing>
                <property name="name">page1</property>
                <property name="title" translatable="yes">Quick Steps</property>
                <property name="position">1</property>
              </packing>
            </child>
            <child>
              <object class="GtkBox" id="box_options">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="margin_left">5</property>
                <property name="margin_right">5</property>
                <property name="margin_top">5</property>
                <property name="orientation">vertical</property>
                <property name="baseline_position">top</property>
                <child>
                  <object class="GtkScrolledWindow">
                    <property name="visible">True</property>
                    <property name="can_focus">True</property>
                    <child>
                      <object class="GtkViewport">
                        <property name="visible">True</property>
                        <property name="can_focus">False</property>
                        <property name="vexpand">True</property>
                        <property name="shadow_type">none</property>
                        <child>
                          <object class="GtkLabel">
                            <property name="visible">True</property>
                            <property name="can_focus">False</property>
                            <property name="halign">start</property>
                            <property name="valign">start</property>
                            <property name="label" translatable="yes">&lt;span font='11'&gt;&lt;b&gt;&lt;big&gt;Options&lt;/big&gt;&lt;/b&gt;
The 'Options' tab lets you define how you want images to be resized.

* &lt;b&gt;&lt;i&gt;Do not resize&lt;/i&gt;&lt;/b&gt; *
Select this if all you want to do is save the image(s) in a new format, without resizing the images. For example: from PNG to JPG.

* &lt;b&gt;&lt;i&gt;Resize by pixel&lt;/i&gt;&lt;/b&gt; *
Resizes images based on specific pixel dimensions. If &lt;u&gt;Keep Aspect Ratio&lt;/u&gt; is checked, images are resized proportionally, to avoid resized images from being stretched.

* &lt;b&gt;&lt;i&gt;Resize by percentage&lt;/i&gt;&lt;/b&gt; *
This will reduce the pixel dimensions of the images by the specified percentage.
&lt;i&gt;Note: if an image cannot be made smaller, then no actual resize will get done for that image. For example, if an image is 1 pixel (width) by 1 pixel (height), then it cannot be made smaller by 99 percent. OpenResizer will consider such a scenario as an error.&lt;/i&gt;

* &lt;b&gt;&lt;i&gt;Only resize image(s) if the dimensions are larger than:&lt;/i&gt;&lt;/b&gt; *
This will only attempt to resize images that are originally bigger than the specified dimensions. If an image is equal to or smaller than the specified dimensions, it is skipped and a warning will be shown for that image - in the final log window.

* &lt;b&gt;&lt;i&gt;JPG quality (percent)&lt;/i&gt;&lt;/b&gt; *
This setting adjusts the quality of images when they get resized as a JPG. The higher the percent, the better the quality, but the bigger the file size will be. The default is 85 percent.

* &lt;b&gt;&lt;i&gt;CPU utilization&lt;/i&gt;&lt;/b&gt; *
When &lt;u&gt;Single CPU / Core&lt;/u&gt; is selected, only one image is resized at a time. When &lt;u&gt;Multi CPUs / Cores&lt;/u&gt; is selected, multiple images get resized at the same time, matching the number of cores. For example, if you have a quad-core CPU, then 4 images will be resized at the same time.
&lt;/span&gt;</property>
                            <property name="use_markup">True</property>
                            <property name="wrap">True</property>
                            <property name="selectable">True</property>
                          </object>
                        </child>
                      </object>
                    </child>
                  </object>
                  <packing>
                    <property name="expand">True</property>
                    <property name="fill">True</property>
                    <property name="position">0</property>
                  </packing>
                </child>
              </object>
              <packing>
                <property name="name">page2</property>
                <property name="title" translatable="yes">Options</property>
                <property name="position">2</property>
              </packing>
            </child>
            <child>
              <object class="GtkBox" id="box_file_name">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="margin_left">5</property>
                <property name="margin_right">5</property>
                <property name="margin_top">5</property>
                <property name="orientation">vertical</property>
                <property name="baseline_position">top</property>
                <child>
                  <object class="GtkScrolledWindow">
                    <property name="visible">True</property>
                    <property name="can_focus">True</property>
                    <child>
                      <object class="GtkViewport">
                        <property name="visible">True</property>
                        <property name="can_focus">False</property>
                        <property name="vexpand">True</property>
                        <property name="shadow_type">none</property>
                        <child>
                          <object class="GtkLabel">
                            <property name="visible">True</property>
                            <property name="can_focus">False</property>
                            <property name="halign">start</property>
                            <property name="valign">start</property>
                            <property name="label" translatable="yes">&lt;span font='11'&gt;&lt;b&gt;&lt;big&gt;File Name&lt;/big&gt;&lt;/b&gt;
The 'File Name' tab lets you define what the ending of a file name should be, after a resize has been done. It also lets you decide what should happen if a destination file already exists with the same name.

* &lt;i&gt;&lt;b&gt;Destination file name&lt;/b&gt;&lt;/i&gt; *
Here, you can define what the ending of a destination file name should be.

For example, if you specify that the ending of all resized images should be &lt;i&gt;_resized&lt;/i&gt;, then &lt;i&gt;test.jpg&lt;/i&gt; will save as &lt;i&gt;test_resized.jpg&lt;/i&gt; in the destination folder.

* &lt;i&gt;&lt;b&gt;If a destination file already exists:&lt;/b&gt;&lt;/i&gt; *
&lt;u&gt;Save as unique file name&lt;/u&gt;: if a destination file name already exists, it will append a number to the end of the file name, until the file name is unique in the destination folder.
For example, if test_resized.jpg already exists, it will try to save it as &lt;i&gt;test_resized2.jpg&lt;/i&gt;, or &lt;i&gt;test_resized3.jpg&lt;/i&gt;, etc...until the file name is unique.

&lt;u&gt;Skip the image, do not resize it&lt;/u&gt;: if the destination file name already exists, it will be skipped and a warning will be appended to the log.

&lt;u&gt;Overwrite the destination file&lt;/u&gt;: if the destination file name already exists, it will attempt to overwrite it.&lt;/span&gt;</property>
                            <property name="use_markup">True</property>
                            <property name="wrap">True</property>
                            <property name="selectable">True</property>
                          </object>
                        </child>
                      </object>
                    </child>
                  </object>
                  <packing>
                    <property name="expand">True</property>
                    <property name="fill">True</property>
                    <property name="position">0</property>
                  </packing>
                </child>
              </object>
              <packing>
                <property name="name">page3</property>
                <property name="title" translatable="yes">File Name</property>
                <property name="position">3</property>
              </packing>
            </child>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">1</property>
          </packing>
        </child>
      </object>
    </child>
    <child type="titlebar">
      <placeholder/>
    </child>
  </object>
</interface>
