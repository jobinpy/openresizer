# OpenResizer
Batch image resizer with a GUI.

OpenResizer is an open-source batch image resizing software.
It is designed to be fast and easy-to-use.

Some of its features are:
- Supports: BMP, PNG (inculding transparency), JPG (including adjusting the quality level).
- Append some text to resized file names (example: myimage.jpg can become myimage_resized.jpg).
- Drag and drop images.
- Ability to save resized images to a new folder.
- Supports multiple CPU's and Cores to speed up batch image resizing.


Official website: www.openresizer.org